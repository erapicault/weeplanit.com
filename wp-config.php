<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'weeplanit_com_3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mS-BkLVbrbH{Q]L#:f4=[lb*/ZqwSA^0W+JyAkJ{qQ3B}{LH6JR1|AhOD]W;l<s;');
define('SECURE_AUTH_KEY',  '$L>1JC*@J}k]Kt)s,A-K<O->+_ / X1@U(Va7#;]~!zR-=fcM:]:y0L{t#:mszNM');
define('LOGGED_IN_KEY',    'nPiMa.=5hR0kAPlp5-tLvw<Pv+viM>WaMz$Q.3=M-8(Dn0*2CrW+*UHfI5ULCNKJ');
define('NONCE_KEY',        '+@2:o+-BoyvJ&S$C]qJ>:67fd~*%d7G1]v2B^; g4fJ58SD?R`4BQ,R`y~xX)8!2');
define('AUTH_SALT',        'fx@`*)>pjahbB?C`TM~IkFlC+reDBMzzZIpzbluR/1H(n1%~lc.QAZ|jhP. /rv!');
define('SECURE_AUTH_SALT', 'y/2+DEko0uN,D-BaY$]5{+ePt6OO}MH3Vr3PfyXHlE<zVnEw6vIYqf,]+R:e]Jb?');
define('LOGGED_IN_SALT',   ')U@v|^ae>bg8zY~-B%9eLVFjrVipzxv}77x$|CeF%>TLRz#{*>|D$FNn{*`#kX!:');
define('NONCE_SALT',       'P[&*+JTsXAa$5|}N]qZ~/H?09/tM6,gU)|[0)+ ,O^hW$L #[;KaDyG_WC@bsA1u');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_xaezg3_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
