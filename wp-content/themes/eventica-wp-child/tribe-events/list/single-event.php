<?php
/**
 * Map View Single Event
 * This file contains one event in the map
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/map/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
        die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();
if ( isset( $venue_details['linked_name'] ) ) {
        $venue_name = $venue_details['linked_name'];
}
elseif ( isset( $venue_details['name'] ) ) {
        $venue_name = $venue_details['name'];
}
else {
        $venue_name = '';
}
global $post;
// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? 'location': '';

// Organizer
$organizer = tribe_get_organizer();
$thumb_venue ="";
$organizer_id = tribe_get_organizer_id();
if($organizer_id)
        $thumb_venue = tribe_event_featured_image( $organizer_id, 'medium', false );
if($thumb_venue == "")
        $thumb_venue = tribe_event_featured_image( tribe_get_venue_id(), 'medium', false );
if($thumb_venue == ""){
        $thumb_venue = '<div class="tribe-events-event-image"><img width="50" height="50" src="'.get_stylesheet_directory_uri().'/images/thumb-venue.png" class="attachment-medium size-medium wp-post-image" alt=""></div>';
}
global $current_language;
global $current_category;
global $weeplanit_translations;
global $post;
$main_image = "";
$addclass = '';

$imgcolclass = 'col-xs-12 col-sm-3';
$infocolclass = 'col-xs-12 col-sm-12';
$calltoactionImg = 'hidden';
$calltoactionInfo = '';

if(has_post_thumbnail()){
                $main_image = get_the_post_thumbnail_url();             
 }elseif(has_post_thumbnail(tribe_get_venue_id())) {
                $main_image = get_the_post_thumbnail_url(tribe_get_venue_id());
                $addclass = 'logo';

}elseif(has_post_thumbnail(tribe_get_organizer_id())) {
                $main_image = get_the_post_thumbnail_url(tribe_get_organizer_id());
                $addclass = 'logo';
}else{
         $main_image = get_template_directory_uri() . "/images/thumb-event.png";
         
 }      
    
    
    
?>  
<div class="even-list-wrapper clearfix">

        <div class="event-list-wrapper-top col-xs-12 col-sm-3">
                <div class="tribe-events-event-image thumbnail">


                        <a onClick="select_event('<?php echo $post->post_name;?>');" href="<?php echo tribe_get_event_website_url(); ?>" target="_blank" title="<?php the_title(); ?>" class="activity-thumbnail <?php echo $addclass; ?> ev-event-thumbnail" style="background-image:url(<?php echo $main_image; ?>);">
                        </a>

                </div>

        </div>

        <div class="event-list-wrapper-bottom col-xs-12 col-sm-9">
                <div class="wraper-bottom-left">

                                <div class="" style="float:right;width:60px;height: 65px;">
                                         <?php echo $thumb_venue ?>
                                </div>
                                <div class="float:left;">
                                        <h2 class="tribe-events-list-event-title entry-title summary">
                                <a href="<?php echo tribe_get_event_website_url(); ?>" onClick="select_event('<?php echo $post->post_name;?>');" target="_blank" class="url ev-event-title" title="<?php the_title() ?>" rel="bookmark">
                                        <?php the_title() ?>
                                </a>
                        </h2>
                                        <?php echo tribe_events_event_schedule_details(); ?> 
                                        <?php echo wp_kses_data(tribe_get_venue()); ?> -
                                        <?php echo wp_kses_data(tribe_get_city()); ?>
                                        <?php echo tribe_get_map_link_html();?>



                                </div>

                                <div class="col-xs-12 col-sm-12" style="padding:0;">
                                <div class="categories" >
                                <div class="col-xs-12 col-sm-12" style="padding:0;">
                                    <?php weeplanit_the_tags();?>
                                </div>
                        </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 event-excerpt">
<?php the_excerpt(); ?>
                </div>
                </div>

        </div>


</div>
