<?php

function weeplanit_categories_rss( $list, $type ) {
	$post_parent = wp_get_post_parent_id(get_the_ID());
	$current_year = date("Y");
	$list = "";
	$latlng = tribe_get_coordinates();
	$list = '<parent>'.$post_parent.'</parent><category>' . get_the_ID() . '</category><latlng>'.$latlng["lat"].','.$latlng["lng"].'</latlng><address>' . tribe_get_address() . ' ' . tribe_get_city() . ' ' . tribe_get_stateprovince() . ' ' . tribe_get_zip() . '</address>';
	$fields = tribe_get_custom_fields();

	$day = $min = $max = $radius = $postalCode = "";
	$priority = "medium";
	$min = $max = $radius = $postalCode = $specialDay = "";
	if (array_key_exists('Promotion Restrictions', $fields)) {
		$restrictions = explode(', ', $fields['Promotion Restrictions']);
		foreach ($restrictions as $restriction) {
			$rest = explode(':', $restriction);
			switch ($rest[0]) {
				case 'min':
					$min = $rest[1];
					break;
				case 'max':
					$max = $rest[1];
					break;
				case 'radius':
					$radius  = $rest[1];
					break;
				case 'postal-code':
					$postalCode = $rest[1];
					break;
				case 'day':
                    $day = $rest[1];
                     break;
				case 'priority':
                    $priority = $rest[1];
                     break;
				default:
					break;
			}
		}
	}
	$list .= '<postalCode>'.$postalCode.'</postalCode>';
	$list .= '<day>'.$specialDay.'</day>';
	$list .= '<radius>'.$radius.'</radius>';
	$list .= '<day>'.$day.'</day>';
	$list .= '<priority>'.$priority.'</priority>';
	if($min != '' && $max != "") {
		for($i = (int)$min; $i<=(int)$max; $i++){
			$list .= '<year>'.($current_year-$i).'</year>';
			$list .= '<category>'.($current_year-$i).'</category>';
		}
	}else{
		$ages = get_post_meta(get_the_ID(), '_ecp_custom_2');
		$ages = array_reverse(explode("|", $ages[0]));
		foreach ($ages as $age){
			$age = (int)$age;
			$list .= '<year>'.($current_year-$age).'</year>';
			$list .= '<category>'.($current_year-$age).'</category>';
		}	
	}
	$list .= '<recurring>'.(tribe_is_recurring_event() ? '1' : '0').'</recurring>';
	return $list;
}
add_filter( 'the_category_rss', 'weeplanit_categories_rss', 10, 2 ); 

function weeplanit_content_rss($content){
	if(get_post_type() == "tribe_events"){
		$weeplanit_language =qtranxf_getLanguage();

		$content = file_get_contents(get_stylesheet_directory() . '/mailchimp-template.html');
		$content = str_replace("|WEEPLANIT_IMAGE_URL|", wp_get_attachment_image_url(get_post_thumbnail_id(null), "mailchimp"), $content);
		$content = str_replace("|WEEPLANIT_TITLE|", get_the_title(), $content);
		$content = str_replace("|WEEPLANIT_DISTANCE|", '*|EVENT'.get_the_ID().'|*', $content);
		$content = str_replace("|WEEPLANIT_COLOR|", 'aaa', $content);
		$content = str_replace("|WEEPLANIT_ORGANIZER|", tribe_get_venue() . (tribe_get_address(null) == '' ? '' : ' - '.tribe_get_address(null)) .', ' . tribe_get_city(), $content);
		$content = str_replace("|WEEPLANIT_DESCRIPTION|", (get_the_excerpt() != "" ? get_the_excerpt() : get_the_content()), $content);
		$content = str_replace("|WEEPLANIT_AGE|", get_age_range(), $content);
		$content = str_replace("|WEEPLANIT_LANGUAGE|", get_language(), $content);

		$content = str_replace("|WEEPLANIT_LINK|", tribe_get_event_website_url(), $content);
		$content = str_replace("|WEEPLANIT_FREE|", (tribe_get_cost(null) == 'Gratuit' || tribe_get_cost(null) == 'Free' ? '<span style="text-align:right;color:#fe7e00"><span style="background-color:#ffffff">&nbsp;'.($weeplanit_language == "fr" ? "gratuit" : "Free").'&nbsp;</span></span>' : ''), $content);
		$content = str_replace("|WEEPLANIT_SMS_LINK|", weeplanit_get_sms_link(), $content);
		$content = str_replace("|WEEPLANIT_SMS_TEXT|", ($weeplanit_language == "fr" ? "SMS" : "SMS"), $content);
		$content = str_replace("|WEEPLANIT_EMAIL_LINK|", weeplanit_get_email_link(), $content);
		$content = str_replace("|WEEPLANIT_EMAIL_TEXT|", ($weeplanit_language == "fr" ? "Transférer" : "Forward"), $content);

		$url = tribe_get_event_link();
		if($url != ""){
			$discover = '<a class="mcnButton " title="En savoir plus " href="'.tribe_get_event_website_url().'" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">'.($weeplanit_language == 'fr' ? 'Visitez le site web' : 'Visit Website').'</a>';
			$content = str_replace("|WEEPLANIT_DISCOVER|", $discover, $content);
		}
		
		$content = str_replace("|WEEPLANIT_TAGS|", getTagTemplate(tribe_get_cost(null, true), '#EF5B5B'). getTagTemplate(get_age_range(), "#FFBA49") . getTagTemplate(get_language(),"#28AFB0"), $content);
		$fields = tribe_get_custom_fields();
       	$str = '<tr>
                        <td style="font-size:11px;line-height:100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">'.tribe_events_event_schedule_details().'</td>
                    </tr>';
        $content = str_replace("|WEEPLANIT_DATES|", tribe_events_event_schedule_details(), $content);  
        $content = str_replace("|WEEPLANIT_COST|", tribe_get_cost(null, true), $content);      

        $str = '<a class="mcnButton " title="'.($weeplanit_language == "fr" ? "Visitez le site web" : "Visit the website").'" href="'.tribe_get_event_link().'" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration:none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">'.($weeplanit_language == "fr" ? "Visitez le site web" : "Visit the website").'</a>';
        $content = str_replace("|WEEPLANIT_LINK|", $str, $content);                

		return $content;
	} 
	return $content;
}
add_filter( "the_content_feed", "weeplanit_content_rss" );

add_filter( "the_excerpt_rss", "weeplanit_excerpt_rss" );
function weeplanit_excerpt_rss($output){
$small_image = "";
if(has_post_thumbnail()){
	$oragnizer_id = tribe_get_organizer_id();
	if($oragnizer_id)
		$small_image = tribe_event_featured_image( $oragnizer_id, 'post-thumbnail', false );
	if($small_image == "")
		$small_image = tribe_event_featured_image( tribe_get_venue_id(), 'post-thumbnail', false );
}

return '<a style="text-decoration: none;" href="'.tribe_get_event_website_url().'">
            <h3 class="null" style="text-align: left;display: block;margin: 0;padding: 0;color: #fe7e00;font-family: \'Lucida Sans Unicode\', \'Lucida Grande\', sans-serif;font-size: 14px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;">' . tribe_event_featured_image( tribe_get_venue_id(), 'post-thumbnail', false ) . get_the_title().(tribe_get_cost(null) == 'Gratuit' ? '<img  height="24" src="'.get_stylesheet_directory_uri() . '/images/free.png" style="border: 0px initial;width: 24px;height: 24px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="24">' : '') .'</h3></a>'.tribe_get_venue() . ' | ' . (tribe_get_cost(null) == 'Gratuit' ? '' : tribe_get_cost(null) . ' | ') . get_age_range() . ' | ' . get_language();
}
function getTagTemplate($tag, $backgroundcolor) {
	if($tag == null || $tag == '') return '';
	return '<!--[if mso]>
                              <td align="center" valign="top">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                       <tr>
                                          <td valign="top" style="padding: 1px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnShareContentItemContainer">
                                             <table border="0" cellpadding="0" cellspacing="0" class="mcnShareContentItem" style="border-collapse: separate;border-radius: 3px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody>
                                                   <tr>
                                                      <td align="left" valign="middle" style="padding:1px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                         <table width="75px" align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-style: solid;border-width: 1px;
                                                            border-color: '.$backgroundcolor.';border-collapse: separate !important;border-radius: 5px;background-color:'.$backgroundcolor.';mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                               <tr style="">
                                                                  <td align="left" valign="middle" class="mcnShareTextContent" style="padding:4px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                     <span style="color: #fff;font-family: &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif;font-size: 10px;font-weight: bold;line-height: normal;text-align: center;text-decoration: none;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">'.$tag.'</span>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->';
}
?>
