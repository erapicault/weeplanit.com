<?php
include( get_stylesheet_directory() . "/function-mailchimp.php");
add_action( 'template_redirect', 'redirect_to_specific_page' );
function redirect_to_specific_page() {
	if(is_feed()) return;
        if (! is_user_logged_in() && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php') {
                wp_redirect( 'https://planner.weeplanit.com/', 301 );
                exit;
        }
}

add_action( 'wp_enqueue_scripts', 'weeplanit_enqueue_styles' );
function weeplanit_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'icon-font', '/wp-content/themes/eventica-wp-child/icon-font.css', array(), '20171028');

}

function wpdocs_child_theme_setup() {
    load_child_theme_textdomain( 'tokopress', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'wpdocs_child_theme_setup' );

add_filter( 'tribe_events_list_the_date_headers', 'weeplanit_tribe_events_list_the_date_headers', 10, 3);
function weeplanit_tribe_events_list_the_date_headers($html, $event_month, $event_year ){
	global $post, $wp_query;
	$month_year_format = tribe_get_date_option( 'monthAndYearFormat', 'F Y' );
	$eventDate = new DateTime($wp_query->query_vars['eventDate']);
	if($eventDate->format('Y-m-d') > tribe_get_start_date($post, false, 'Y-m-d')){
		if ( $wp_query->current_post > 0) {
				$prev_post = $wp_query->posts[ $wp_query->current_post - 1 ];
				if($eventDate->format('Y-m-d') > tribe_get_start_date($prev_post, false, 'Y-m-d'))
					return '';
		}
		return sprintf( "<h2 class='tribe-events-list-separator-month'><span>%s</span></h2>", tribe_format_date($wp_query->query_vars['eventDate'], false, $month_year_format));
	}
	return $html;
}

add_filter('term_link','weeplanit_term_link', 10, 3);
function weeplanit_term_link($termlink, $term, $taxonomy){
	return str_replace('categorie','category', $termlink);
}

add_filter( 'tribe_events_get_link', 'weeplanit_tribe_events_get_link',10,6);
function weeplanit_tribe_events_get_link( $event_url, $type, $secondary, $term, $url_args, $featured){
	$event_url = str_replace("liste","list",$event_url);
	$event_url = str_replace('mois', 'month', $event_url);
	$event_url = str_replace('semaine', 'week', $event_url);
	$event_url = str_replace('aujourdhui', 'today', $event_url);
	$event_url = str_replace('carte', 'map', $event_url);
	$event_url = str_replace('categorie', 'category', $event_url);
	$event_url = str_replace('tous', 'all', $event_url);
	return $event_url;
}

add_filter('tribe_get_event_link', 'weeplanit_tribe_get_event_link', 10, 4);
function weeplanit_tribe_get_event_link($link, $postId, $full_link, $url ){
	return tribe_get_event_website_url();
}

function weeplanit_get_ages(){
	$additional_fields = tribe_get_custom_fields();
	$ages = "";
	$current_language = qtranxf_getLanguage();
	if (array_key_exists('Age', $additional_fields)) {
 		$ages = explode(', ', $additional_fields['Age']);
 		$min = min($ages);
 		$max = max($ages);
 		if($min == 0 && $max == 15)
 			$ages = ($current_language == 'fr' ? "pour tous" : "for all");
 		elseif($max == 15)
 			$ages = $min . ($current_language == 'fr' ? " ans et +" : " and up");
 		elseif($min == 0)
 			$ages = $max . ($current_language == 'fr' ? " ans et -" : " and under");
 		else
 			$ages =  min($ages) . '-' . max($ages) . ($current_language == 'fr' ? " ans" : " y.o.");
	}
 	return $ages;
}

function weeplanit_get_language(){
	$current_language = qtranxf_getLanguage();
 	$additional_fields = tribe_get_custom_fields();
 	if (array_key_exists('Language', $additional_fields)) {
 		$additional_fields['Language'] = str_replace('French, English', 'Bilingual', $additional_fields['Language']);
 		if($current_language == 'fr'){
 			$additional_fields['Language'] = str_replace('French', 'français', $additional_fields['Language']);
 			$additional_fields['Language'] = str_replace('English', 'anglais', $additional_fields['Language']);
 			$additional_fields['Language'] = str_replace('Bilingual', 'bilingue', $additional_fields['Language']);
 		}
 	}
 	if (array_key_exists('Language', $additional_fields)) {
 		return $additional_fields['Language'];
 	}
 	return "";
}

add_filter('tribe_events_event_schedule_details_formatting', 'weeplanit_schedule_formatting');
function weeplanit_schedule_formatting( array $settings ) {
	$current_language = qtranxf_getLanguage();
	$settings['time'] = true;
	$settings['show_end_time'] = true;
	$settings['date_without_year_format'] = $settings['date_with_year_format'] = ($current_language == 'fr' ? 'j F' : 'F jS');
	$settings['time_format'] = ($current_language == 'fr' ? 'G:i' : 'g:i a');
	$settings['datetime_separator'] = tribe_get_option( 'dateTimeSeparator', '<br>' );
	$settings['time_range_separator'] = tribe_get_option( 'timeRangeSeparator', ' - ' );
	return $settings;
}

add_filter( 'tribe_events_recurrence_tooltip', 'weeplanit_tribe_events_recurrence_tooltip', 10, 1);
function weeplanit_tribe_events_recurrence_tooltip($tooltip){
        global $post;
        $recurrence_rules = Tribe__Events__Pro__Recurrence__Meta::getRecurrenceMeta( $post->ID );
        if ( ! empty( $recurrence_rules['description'] ) ) {
                $tooltip = ' ('.$recurrence_rules['description'].')';
                return $tooltip;
        }
        return "";
}

//------------------------------------------------------
// SET END TIME TO ONLY SHOW EVENTS AT THE START DATE
//-------------------------------------------------------
add_filter( 'tribe_events_pre_get_posts', 'set_end_date', 20, 1 );
function set_end_date($query){
	if(is_front_page()){
		$query->set( 'end_date',  tribe_end_of_day(date('Y-m-d', strtotime($query->get( 'start_date'). ' + 5 days'))));	
	}
}


function custom_excerpt_length( $length ) {
   return 30; // show 30 words
}
add_filter( ‘excerpt_length’, ‘custom_excerpt_length’, 999 );

/*function weeplanit_tribe_get_start_end_date($event){
	if ( is_null( $event ) ) {
			global $post;
			$event = $post;
	}
	if ( tribe_is_recurring_event( $event->ID ) ) {
		$meta =  Tribe__Events__Pro__Recurrence__Meta::getRecurrenceMeta($event->ID);
		$start_date = tribe_format_date(get_post_meta( $event->ID, "_EventStartDate", true ), false, 'Y-m-d');
		$end_date = tribe_format_date(get_post_meta( $event->ID, "_EventEndDate", true ), false, 'Y-m-d');
		if($meta["rules"]){
			for($i=0; $i<count($meta["rules"]);$i++){
				if($start_date > $meta["rules"][$i]["EventStartDate"]){
					$start_date = $meta["rules"][$i]["EventStartDate"];
				}
				if($end_date < $meta["rules"][$i]["end"]){
					$end_date = $meta["rules"][$i]["end"];
				}
			}
		}
		return tribe_format_date($start_date, false, null) . '-'. tribe_format_date($end_date, false, null);
	}
	return  tribe_get_start_date( null, false, '', null );
}



function weeplanit_tribe_get_latest_date($event){
	if ( is_null( $event ) ) {
			global $post;
			$event = $post;
	}
	if ( tribe_is_recurring_event( $event->ID ) ) {
		$meta =  Tribe__Events__Pro__Recurrence__Meta::getRecurrenceMeta($event->ID);
		$end_date = tribe_format_date(get_post_meta( $event->ID, "_EventEndDate", true ), false, 'Y-m-d');
		if($meta["rules"]){
			for($i=0; $i<count($meta["rules"]);$i++){
				if($end_date < $meta["rules"][$i]["end"]){
					$end_date = $meta["rules"][$i]["end"];
				}
			}
		}
		return tribe_format_date($end_date, false, null);
	}
	return  '';
}*/

function weeplanit_the_tags(){
	$links = array();
	if(weeplanit_get_ages() != '')
		$links[] = '<div class="event-tag event-tag-ages">'.weeplanit_get_ages().'</div>';
	if(weeplanit_get_language() != '')
	$links[] = '<div class="event-tag event-tag-language">'.weeplanit_get_language().'</div>';
	if(tribe_get_cost( null, false ) == 'Free' || tribe_get_cost( null, false ) == 'Gratuit'){
		$links[] = '<div class="event-tag event-tag-free">Free</div>';
	} 
	$tags = get_the_terms( $id, 'post_tag');

    foreach($tags as $tag) {
        $links[] = '<div class="event-tag event-tag-category event-tag-category-'.$tag->slug.'">'.$tag->name.'</div>';
   }
	echo join($links );
}

//add_filter( 'pre_term_link', 'weeplanit_pre_term_link', 10, 2);
function weeplanit_pre_term_link($termlink, $term ){
	return '';
}

//dd_filter('tribe_get_start_date', 'weeplanit_tribe_get_start_date', 10, 2);
function weeplanit_tribe_get_start_date($formatted_date, $event){
	if ( is_null( $event ) ) {
			global $post;
			$event = $post;
	}
	if ( tribe_is_recurring_event( $event->ID ) ) {
		$start_date = tribe_format_date(get_post_meta( $event->ID, "_EventStartDate", true ), false, 'Y-m-d');
		if($meta["rules"]){
			for($i=0; $i<count($meta["rules"]);$i++){
				if($start_date > $meta["rules"][$i]["EventStartDate"]){
					$start_date = $meta["rules"][$i]["EventStartDate"];
				}
			}
			return tribe_format_date($start_date, false, null);
		}
		
	}
	return $formatted_date;
}

//add_filter('tribe_get_end_date', 'weeplanit_tribe_get_end_date', 10, 2);
function weeplanit_tribe_get_end_date($formatted_date, $event){
	if ( is_null( $event ) ) {
			global $post;
			$event = $post;
	}
	if ( tribe_is_recurring_event( $event->ID )) {
		$end_date = tribe_format_date(get_post_meta( $event->ID, "_EventEndDate", true ), false, 'Y-m-d');
		if($meta["rules"]){
			for($i=0; $i<count($meta["rules"]);$i++){
				if($end_date < $meta["rules"][$i]["end"]){
					$end_date = $meta["rules"][$i]["end"];
				}
			}
			return tribe_format_date($end_date, true, null);
		}
		
	}
	return $formatted_date;
}


?>
