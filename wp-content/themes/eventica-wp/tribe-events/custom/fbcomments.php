<?php
if ( ! tokopress_get_mod( 'tokopress_events_fbcomments_show' ) )
	return;

$title = tokopress_get_mod( 'tokopress_events_fbcomments_custom_title' );
if ( !trim($title) ) {
	$title = esc_html__( 'Facebook Comments', 'tokopress' );
} 

?>

<div class="section-wrap event-fbcomments-wrap">
	<div class="section-title event-fbcomments-title">
		<h2 class="section-heading"><?php echo esc_html( $title ); ?></h2>
	</div>

	<div class="section-area event-fbcomments-area">
		<div class="fb-comments" data-href="<?php echo wp_get_shortlink(); ?> " data-width="100%" data-numposts="5"></div>
	</div>
</div>
