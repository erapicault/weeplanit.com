<?php

if ( tokopress_get_mod( 'tokopress_events_hide_single_related' ) )
	return;

$posts_per_page = tokopress_get_mod( 'tokopress_events_related_postsperpage' );
if ( ! $posts_per_page ) {
	$posts_per_page = 6;
}

$cats = wp_get_post_terms( get_the_ID(), 'tribe_events_cat', array( 'fields' => 'ids' ) );
$tags = wp_get_post_terms( get_the_ID(), 'post_tag', array( 'fields' => 'ids' ) );

if( empty( $cats ) && empty( $tags ) )
	return;

global $tp_sidebar;
if ( $tp_sidebar ) {
	$class = 'col-sm-6';
}
else {
	$class = 'col-md-4 col-sm-6';
}

$args = array(
		'post_type'			=> 'tribe_events',
		'posts_per_page'	=> $posts_per_page,
		'post__not_in'		=> array( get_the_ID() ),
		'eventDisplay' 		=> 'list',
		'tax_query' 		=> array('relation' => 'OR'),
		'orderby'			=>'rand',
	);
if ( tokopress_get_mod( 'tokopress_events_include_past_related' ) ) {
	$args['eventDisplay'] = 'custom';
}
else {
	$args['eventDisplay'] = 'list';
}
if ( !empty( $cats ) ) {
	$args['tax_query'][] = array(
			'taxonomy'=> 'tribe_events_cat',
			'terms'=> $cats,
			'field'=> 'id'
		);

}
if ( !empty( $tags ) ) {
	$args['tax_query'][] = array(
			'taxonomy'=> 'post_tag',
			'terms'=> $tags,
			'field'=> 'id'
		);

}
$query = new WP_Query( $args );

$related_title = tokopress_get_mod( 'tokopress_events_custom_related_title' );
if ( !trim($related_title) ) {
	$related_title = esc_html__( 'Related Events', 'tokopress' );
} 

?>

<?php if( $query->have_posts() ) : ?>

	<div class="section-wrap related-event-wrap tribe-events-list">
		<div class="section-title related-event-title">
			<h2 class="section-heading"><?php echo esc_html( $related_title ); ?></h2>
		</div>

		<div class="events-loop">
			<div class="row">
				<?php while ( $query->have_posts() ) : ?>
					<?php $query->the_post(); ?>
					
					<div id="post-<?php the_ID() ?>" class="<?php tribe_events_event_classes() ?> <?php echo esc_attr( $class ); ?>">
						<?php tribe_get_template_part( 'list/single', 'event' ); ?>
					</div>
							
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
			</div>
		</div>

	</div>

<?php endif; ?>