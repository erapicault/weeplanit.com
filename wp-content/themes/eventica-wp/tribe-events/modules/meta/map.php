<?php
/**
 * Single Event Meta (Map) Template
 *
 * Override this template in your own theme by creating a file at:
 * [your-theme]/tribe-events/modules/meta/details.php
 *
 * @package TribeEventsCalendar
 */

if ( !tribe_embed_google_map() )
	return;

$map_api_key = tribe_get_option( 'google_maps_js_api_key' );
$map_width = apply_filters( 'tribe_events_single_map_default_width', '100%' );
$map_height = apply_filters( 'tribe_events_single_map_default_height', '350px' );
$map_zoom = apply_filters( 'tribe_events_single_map_zoom_level', (int) tribe_get_option( 'embedGoogleMapsZoom', 8 ) );
$venue_id = tribe_get_venue_id( get_the_ID() );

?>

<div class="tribe-events-meta-group tribe-events-meta-group-gmap clearfix">
	<div class="tribe-events-venue-map" style="height:<?php echo esc_attr($map_height); ?>">
		<?php
		do_action( 'tribe_events_single_meta_map_section_start' );
		if ( ! empty( $map_api_key ) ) {
			echo apply_filters( 'tribe_event_meta_venue_map', tribe_get_embedded_map( $venue_id ) );
		}
		else {
			$map_address = '';
			$location_parts = array( 'address', 'city', 'state', 'province', 'zip', 'country' );
			foreach ( $location_parts as $val ) {
				$address_part = call_user_func( 'tribe_get_' . $val, $venue_id );
				if ( $address_part ) {
					$map_address .= $address_part . ' ';
				}
			}
			if ( class_exists( 'Tribe__Events__Pro__Geo_Loc' ) && empty( $map_address ) ) {
				$overwrite = (int) get_post_meta( $this->venue_id, Tribe__Events__Pro__Geo_Loc::OVERWRITE, true );
				if ( $overwrite ) {
					$lat = get_post_meta( $venue_id, Tribe__Events__Pro__Geo_Loc::LAT, true );
					$lng = get_post_meta( $venue_id, Tribe__Events__Pro__Geo_Loc::LNG, true );
					$map_address = $lat . ',' . $lng;
				}
			}
			if ( ! empty( $map_address ) ) {
				echo '<iframe width="'.$map_width.'" height="'.$map_height.'" src="https://maps.google.com/maps?width='.$map_width.'&amp;height='.$map_height.'&amp;q='.rawurlencode($map_address).'&amp;ie=UTF8&amp;t=&amp;z='.$map_zoom.'&amp;iwloc=near&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>';
			}
		}
		do_action( 'tribe_events_single_meta_map_section_end' );
		?>
	</div>
</div>
