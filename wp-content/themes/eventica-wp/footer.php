		<?php 
		if( !tokopress_get_mod( 'tokopresss_disable_footer_widget' ) ) :
			get_template_part( 'block-footer-widget' ); 
		endif;
		?>

		<?php 
		if( !tokopress_get_mod( 'tokopresss_disable_footer_buttom' ) ) : 
			get_template_part( 'block-footer-credit' ); 
		endif;
		?>

		</div>
		<div id="back-top" style="display:block;"><i class="fa fa-angle-up"></i></div>
		<?php if ( tokopress_get_mod( 'tokopress_offcanvas_position' ) == 'right' ) : ?>
			<div class="sb-slidebar sb-right sb-style-push"></div>
		<?php else : ?>
			<div class="sb-slidebar sb-left sb-style-push"></div>
		<?php endif; ?>
		<?php wp_footer(); ?>
	</body>
</html>
