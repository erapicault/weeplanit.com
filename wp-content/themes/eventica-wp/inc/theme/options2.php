<?php 

add_filter( 'tokopress_customize_setting_db', 'tokopress_customize_setting_db_filter' );
function tokopress_customize_setting_db_filter( $value ) {
	return THEME_NAME;
}

add_filter( 'tokopress_customize_assets_uri', 'tokopress_customize_assets_uri_filter' );
function tokopress_customize_assets_uri_filter( $value ) {
	return get_template_directory_uri();
}

add_action( 'customize_register', 'tokopress_customize_reposition_options', 20 );
function tokopress_customize_reposition_options( $wp_customize ) {
	$title = $wp_customize->get_section( 'title_tagline' )->title;
	$wp_customize->get_section( 'title_tagline' )->title = $title.' &amp; '.esc_html__( 'Favicon', 'tokopress' );

	$site_icon = $wp_customize->get_control( 'site_icon' )->label;
	$wp_customize->get_control( 'site_icon' )->label = $site_icon.' / '.esc_html__( 'Favicon', 'tokopress' );

	$wp_customize->remove_control('display_header_text');

	$wp_customize->get_section( 'header_image' )->panel = 'tokopress_options';	
	$wp_customize->get_section( 'header_image' )->priority = 5;
	$wp_customize->get_section( 'header_image' )->title = esc_html__( 'Header - Page Title', 'tokopress' );
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_panels' );
function tokopress_customize_controls_options_panels( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'panel',
		'setting'  => 'tokopress_options',
		'title'    => esc_html__( 'TP - Theme Options', 'tokopress' ),
		'priority' => 22,
	);

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'panel',
		'setting'  => 'tokopress_pagetemplates',
		'title'    => esc_html__( 'TP - Page Templates', 'tokopress' ),
		'priority' => 26,
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_headertop' );
function tokopress_customize_controls_options_headertop( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_headertop',
		'title'    => esc_html__( 'Header - Top (Logo & Menu)', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 3,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_sticky_header_heading', 
		'label'		=> esc_html__( 'Sticky Header', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_sticky_header', 
		'label'		=> esc_html__( 'ENABLE sticky header', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_site_logo_heading', 
		'label'		=> esc_html__( 'Site Logo', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'image',
		'setting'	=> 'tokopress_site_logo', 
		'label'		=> esc_html__( 'Site Logo', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_minicart_heading', 
		'label'		=> esc_html__( 'Minicart', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_minicart_header', 
		'label'		=> esc_html__( 'ENABLE minicart icon on header menu', 'tokopress' ),
		'section'	=> 'tokopress_options_headertop',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_headertop_goto_colors', 
		'label'		=> esc_html__( 'More ...', 'tokopress' ),
		'description' => '<p><span class="dashicons dashicons-admin-appearance"></span> <a href="javascript:wp.customize.section( \'tokopress_colors_headertop\' ).focus();">'.esc_html__( 'Go to Theme Colors of this section', 'tokopress' ).'</a></p>',
		'section'	=> 'tokopress_options_headertop',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_headerpagetitle' );
function tokopress_customize_controls_options_headerpagetitle( $controls ) {

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_page_title_disable', 
		'label'		=> esc_html__( 'DISABLE page title section globally.', 'tokopress' ),
		'section'	=> 'header_image',
		'priority' => 5,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_header_image_heading', 
		'label'		=> esc_html__( 'Page Title Background Image', 'tokopress' ),
		'section'	=> 'header_image',
		'priority' => 9,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_pageheader_goto_colors', 
		'label'		=> esc_html__( 'More ...', 'tokopress' ),
		'description' => '<p><span class="dashicons dashicons-admin-appearance"></span> <a href="javascript:wp.customize.section( \'tokopress_colors_pagetitle\' ).focus();">'.esc_html__( 'Go to Theme Colors of this section', 'tokopress' ).'</a></p>',
		'section'	=> 'header_image',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_headerscripts' );
function tokopress_customize_controls_options_headerscripts( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_headerscripts',
		'title'    => esc_html__( 'Header - Scripts', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'textarea-unfiltered',
		'setting'	=> 'tokopress_header_script', 
		'label'		=> esc_html__( 'Header Scripts', 'tokopress' ),
		'section'	=> 'tokopress_options_headerscripts',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_blog' );
function tokopress_customize_controls_options_blog( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_blog',
		'title'    => esc_html__( 'Blog Page', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_blog_hide_sidebar',
		'label'		=> esc_html__( 'DISABLE sidebar on blog page.', 'tokopress' ),
		'section'	=> 'tokopress_options_blog',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_post' );
function tokopress_customize_controls_options_post( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_post',
		'title'    => esc_html__( 'Single Post', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_post_hide_sidebar',
		'label'		=> esc_html__( 'DISABLE sidebar on single post.', 'tokopress' ),
		'section'	=> 'tokopress_options_post',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_post_hide_image',
		'label'		=> esc_html__( 'DISABLE featured image on single post.', 'tokopress' ),
		'section'	=> 'tokopress_options_post',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'radio',
		'setting'	=> 'tokopress_post_meta_position',
		'label'		=> esc_html__( 'Post Meta Position', 'tokopress' ),
		'default'	=> 'side',
		'choices'	=> array( 
			'side'		=> esc_html__( 'Side (Left)', 'tokopress' ),
			'bottom'	=> esc_html__( 'Bottom', 'tokopress' ),
			'hide'		=> esc_html__( 'Disabled', 'tokopress' ),
		),
		'section'	=> 'tokopress_options_post',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_footerwidgets' );
function tokopress_customize_controls_options_footerwidgets( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_footerwidgets',
		'title'    => esc_html__( 'Footer - Widgets', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopresss_disable_footer_widget', 
		'label'		=> esc_html__( 'DISABLE footer widgets area', 'tokopress' ),
		'section'	=> 'tokopress_options_footerwidgets',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_footerwidgets_goto_colors', 
		'label'		=> esc_html__( 'More ...', 'tokopress' ),
		'description' => '<p><span class="dashicons dashicons-admin-appearance"></span> <a href="javascript:wp.customize.section( \'tokopress_colors_footerwidgets\' ).focus();">'.esc_html__( 'Go to Theme Colors of this section', 'tokopress' ).'</a></p><p><span class="dashicons dashicons-admin-generic"></span> <a href="javascript:wp.customize.panel( \'widgets\' ).focus();">'.esc_html__( 'Setup Footer Widgets', 'tokopress' ).'</a></p>',
		'section'	=> 'tokopress_options_footerwidgets',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_footercredits' );
function tokopress_customize_controls_options_footercredits( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_footercredits',
		'title'    => esc_html__( 'Footer - Credits', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopresss_disable_footer_buttom', 
		'label'		=> esc_html__( 'DISABLE footer credits area', 'tokopress' ),
		'section'	=> 'tokopress_options_footercredits',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'textarea-html',
		'setting'	=> 'tokopress_footer_text', 
		'label'		=> esc_html__( 'Footer Credit Text', 'tokopress' ),
		'section'	=> 'tokopress_options_footercredits',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_footer_social_heading', 
		'label'		=> esc_html__( 'Footer - Social Icons', 'tokopress' ),
		'section'	=> 'tokopress_options_footercredits',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_hide_social', 
		'label'		=> esc_html__( 'DISABLE Social Icons', 'tokopress' ),
		'section'	=> 'tokopress_options_footercredits',
	);

	$socials = array(
		'' 				=> esc_html__( 'no icon', 'tokopress' ),
		'rss' 			=> 'RSS Feed',
		'envelope-o' 	=> 'E-mail',
		'twitter' 		=> 'Twitter',
		'facebook' 		=> 'Facebook',
		'google-plus' 	=> 'gPlus',
		'youtube' 		=> 'Youtube',
		'flickr' 		=> 'Flickr',
		'linkedin' 		=> 'Linkedin',
		'pinterest' 	=> 'Pinterest',
		'dribbble' 		=> 'Dribbble',
		'github' 		=> 'Github',
		'lastfm' 		=> 'LastFm',
		'vimeo-square' 	=> 'Vimeo',
		'tumblr' 		=> 'Tumblr',
		'instagram' 	=> 'Instagram',
		'soundcloud' 	=> 'Sound Cloud',
		'behance' 		=> 'Behance',
		'deviantart' 	=> 'Daviant Art'
	);

	for( $is=1;$is<=5;$is++ ) {
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'select',
			'setting'	=> 'tokopress_social_' . $is, 
			'label'		=> sprintf( esc_html__( 'Social Icon #%s', 'tokopress' ), $is ),
			'section'	=> 'tokopress_options_footercredits',
			'choices'   => $socials,
		);
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_social_' . $is . '_url', 
			'label'		=> sprintf( esc_html__( 'Social Icon #%s URL', 'tokopress' ), $is ),
			'section'	=> 'tokopress_options_footercredits',
		);
	}

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_footercredits_goto_colors', 
		'label'		=> esc_html__( 'More ...', 'tokopress' ),
		'description' => '<p><span class="dashicons dashicons-admin-appearance"></span> <a href="javascript:wp.customize.section( \'tokopress_colors_footercredits\' ).focus();">'.esc_html__( 'Go to Theme Colors of this section', 'tokopress' ).'</a></p>',
		'section'	=> 'tokopress_options_footercredits',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_footerscripts' );
function tokopress_customize_controls_options_footerscripts( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_footerscripts',
		'title'    => esc_html__( 'Footer - Scripts', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'textarea-unfiltered',
		'setting'	=> 'tokopress_footer_script', 
		'label'		=> esc_html__( 'Footer Scripts', 'tokopress' ),
		'section'	=> 'tokopress_options_footerscripts',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_offcanvas' );
function tokopress_customize_controls_options_offcanvas( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_offcanvas',
		'title'    => esc_html__( 'Off Canvas Menu', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'radio-buttonset',
		'setting'	=> 'tokopress_offcanvas_position', 
		'label'		=> esc_html__( 'Off Canvas Menu Position', 'tokopress' ),
		'section'	=> 'tokopress_options_offcanvas',
		'default'	=> 'left',
		'choices'	=> array( 
			'left' => esc_html__( 'Left', 'tokopress' ),
			'right' => esc_html__( 'Right', 'tokopress' ),
		),
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_pagetemplates_home' );
function tokopress_customize_controls_options_pagetemplates_home( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_home_templates',
		'title'    => esc_html__( 'Homepage Page Template', 'tokopress' ),
		'panel'    => 'tokopress_pagetemplates',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'warning',
		'setting'	=> 'tokopress_options_home_warning', 
		'label'		=> sprintf( esc_html__( 'You are not in %s', 'tokopress' ), esc_html__( 'Page with Homepage Page Template', 'tokopress' ) ),
		'description' => sprintf( esc_html__( 'These settings only affect %s. Please visit %s to see live preview for these settings.', 'tokopress' ), esc_html__( 'Page with Homepage Page Template', 'tokopress' ) , esc_html__( 'Page with Homepage Page Template', 'tokopress' ) ),
		'section'	=> 'tokopress_options_home_templates',
		'active_callback' => 'tokopress_callback_is_not_home_page',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_home_heading_desc', 
		'label'		=> '',
		'description' => esc_html__( 'These options are for everyone who use Homepage Page Template. If you use Visual Composer to build your homepage, please IGNORE these options.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	if( class_exists( 'Tribe__Events__Main' ) ) {

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'heading',
			'setting'	=> 'tokopress_home_heading_slider', 
			'label'		=> esc_html__( 'Events Slider', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_home_slider_disable', 
			'label'		=> esc_html__( 'DISABLE Events Slider', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slide_button', 
			'label'		=> esc_html__( 'Button Text', 'tokopress' ),
			'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Detail', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slider_ids', 
			'label'		=> esc_html__( 'Show Specific Events By Event IDs', 'tokopress' ),
			'description'=> esc_html__( 'Put event IDs here, separated by comma. Leave it empty if you want to show upcoming events.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'number',
			'setting'	=> 'tokopress_home_slider_numbers', 
			'label'		=> esc_html__( 'Number of Events To Show', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'active_callback' => 'tokopress_callback_home_event_slider_not_by_ids',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slider_category', 
			'label'		=> esc_html__( 'Include Events From Event Categories', 'tokopress' ),
			'description'=> esc_html__( 'Put event category names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'active_callback' => 'tokopress_callback_home_event_slider_not_by_ids',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slider_category_exclude', 
			'label'		=> esc_html__( 'Exclude Events From Event Categories', 'tokopress' ),
			'description'=> esc_html__( 'Put event category names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'active_callback' => 'tokopress_callback_home_event_slider_not_by_ids',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slider_tag', 
			'label'		=> esc_html__( 'Include Events From Event Tags', 'tokopress' ),
			'description'=> esc_html__( 'Put event tag names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'active_callback' => 'tokopress_callback_home_event_slider_not_by_ids',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_slider_tag_exclude', 
			'label'		=> esc_html__( 'Exclude Events From Event Tags', 'tokopress' ),
			'description'=> esc_html__( 'Put event tag names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'active_callback' => 'tokopress_callback_home_event_slider_not_by_ids',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'heading',
			'setting'	=> 'tokopress_home_heading_search', 
			'label'		=> esc_html__( 'Search Form', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_home_search_disable', 
			'label'		=> esc_html__( 'DISABLE Search Form', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_search_text', 
			'label'		=> esc_html__( 'Placeholder Text', 'tokopress' ),
			'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Search Event &hellip;', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'heading',
			'setting'	=> 'tokopress_home_heading_upcoming_events', 
			'label'		=> esc_html__( 'Upcoming Events', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_home_upcoming_event_disable', 
			'label'		=> esc_html__( 'DISABLE Upcoming Events', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event', 
			'label'		=> esc_html__( 'Section Title', 'tokopress' ),
			'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Upcoming Events', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_text', 
			'label'		=> esc_html__( 'Link Text', 'tokopress' ),
			'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'All Events', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'number',
			'default'	=> 3,
			'setting'	=> 'tokopress_home_upcoming_event_numbers', 
			'label'		=> esc_html__( 'Number of Events To Show', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_exclude', 
			'label'		=> esc_html__( 'Exclude Events By Event IDs', 'tokopress' ),
			'description' => esc_html__( 'Insert event IDs, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_category', 
			'label'		=> esc_html__( 'Include Events From Event Categories', 'tokopress' ),
			'description' => esc_html__( 'Put event category names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_category_exclude', 
			'label'		=> esc_html__( 'Exclude Events From Event Categories', 'tokopress' ),
			'description' => esc_html__( 'Put event category names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_tag', 
			'label'		=> esc_html__( 'Include Events From Event Tags', 'tokopress' ),
			'description' => esc_html__( 'Put event tag names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_upcoming_event_tag_exclude', 
			'label'		=> esc_html__( 'Exclude Events From Event Tags', 'tokopress' ),
			'description' => esc_html__( 'Put event tag names here, separated by comma.', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'heading',
			'setting'	=> 'tokopress_home_heading_featured_event', 
			'label'		=> esc_html__( 'Featured Single Event', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'setting'	=> 'tokopress_home_featured_event', 
			'label'		=> esc_html__( 'Section Title', 'tokopress' ),
			'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Featured Event', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
		);

		// wp_count_posts doesn't work here :(
		global $wpdb;
		$post_type = 'tribe_events';
		$query = "SELECT COUNT( * ) FROM {$wpdb->posts} WHERE post_type = %s AND post_status = 'publish'";
		$count_events = $wpdb->get_var( $wpdb->prepare( $query, $post_type ) );

		if ( $count_events <= 50 ) {
			$event_id_type = 'dropdown';
			$event_id_desc = __( 'Select an event for featured event.', 'tokopress' );
			$args = array(
				'post_status'		=> 'publish',
				'posts_per_page'	=> 100,
				'post_type'			=> $post_type,
				'eventDisplay'		=> 'custom',
			);
			$events = get_posts( $args );
			$eventslist = array();
			$eventslist[''] = '';
			foreach ($events as $event) {
				$eventslist[$event->ID] = $event->post_title.' (#'.$event->ID.')';
			}
			$controls[] = array( 
				'setting_type' => 'option_mod',
				'type' 		=> 'select',
				'setting'	=> 'tokopress_home_featured_event_page', 
				'label'		=> esc_html__( 'Event ID', 'tokopress' ),
				'description'=> esc_html__( 'Select an event for featured event.', 'tokopress' ),
				'section'	=> 'tokopress_options_home_templates',
				'choices'   => $eventslist,
			);
		}
		else {
			$controls[] = array( 
				'setting_type' => 'option_mod',
				'type' 		=> 'text',
				'setting'	=> 'tokopress_home_featured_event_page', 
				'label'		=> esc_html__( 'Event ID', 'tokopress' ),
				'description'=> esc_html__( 'Put event ID here for featured event.', 'tokopress' ),
				'section'	=> 'tokopress_options_home_templates',
			);
		}

		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'radio',
			'default'   => 'content',
			'setting'	=> 'tokopress_home_featured_event_content', 
			'label'		=> esc_html__( 'Featured Event Details', 'tokopress' ),
			'section'	=> 'tokopress_options_home_templates',
			'choices' 	=> array(
					'content' => esc_html__( 'Full Content', 'tokopress' ),
					'excerpt' => esc_html__( 'Excerpt / Summary', 'tokopress' ),
				),
		);

	}

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_home_heading_recent_post', 
		'label'		=> esc_html__( 'Recent Updates (Blog)', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_home_recent_post_disable', 
		'label'		=> esc_html__( 'DISABLE Recent Updates (Blog)', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post', 
		'label'		=> esc_html__( 'Section Title', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Recent Updates', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_text', 
		'label'		=> esc_html__( 'Link Text', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'All Posts', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'number',
		'default'	=> 3,
		'setting'	=> 'tokopress_home_recent_post_numbers', 
		'label'		=> esc_html__( 'Number of Posts To Show', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_exclude', 
		'label'		=> esc_html__( 'Exclude Posts By Post IDs', 'tokopress' ),
		'description' => esc_html__( 'Insert post IDs, separated by comma.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_category', 
		'label'		=> esc_html__( 'Include Posts From Post Categories', 'tokopress' ),
		'description' => esc_html__( 'Put post category names here, separated by comma.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_category_exclude', 
		'label'		=> esc_html__( 'Exclude Posts From Post Categories', 'tokopress' ),
		'description' => esc_html__( 'Put post category names here, separated by comma.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_tag', 
		'label'		=> esc_html__( 'Include Posts From Post Tags', 'tokopress' ),
		'description' => esc_html__( 'Put post tag names here, separated by comma.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_recent_post_tag_exclude', 
		'label'		=> esc_html__( 'Exclude Posts From Post Tags', 'tokopress' ),
		'description' => esc_html__( 'Put post tag names here, separated by comma.', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_home_heading_subscribe', 
		'label'		=> esc_html__( 'Subscribe Form', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_home_subscribe_disable', 
		'label'		=> esc_html__( 'DISABLE Subscribe Form', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_subscribe_title', 
		'label'		=> esc_html__( 'Section Title', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Subscribe to our newsletter', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_subscribe_text', 
		'label'		=> esc_html__( 'Section Description', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'never miss our latest news and events updates', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	if ( function_exists('mc4wp_get_forms') ) {
		$forms = mc4wp_get_forms();
		$mailchimp_forms = array( '' => '&nbsp;' );
		foreach( $forms as $form ) {
			$mailchimp_forms[ $form->ID ] = $form->name;
		}
		if ( !empty( $mailchimp_forms ) ) {
			$controls[] = array( 
				'setting_type' => 'option_mod',
				'type' 		=> 'select',
				'setting'	=> 'tokopress_home_subscribe_id', 
				'label'		=> esc_html__( 'Select Mailchimp Form', 'tokopress' ),
				'section'	=> 'tokopress_options_home_templates',
				'choices'	=> $mailchimp_forms
			);
		}
	}

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_home_heading_testimonials', 
		'label'		=> esc_html__( 'Testimonials', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopresss_home_testimonials_disable', 
		'label'		=> esc_html__( 'DISABLE Testimonials', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_home_testimonials', 
		'label'		=> esc_html__( 'Section Title', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Testimonials', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_home_heading_brands', 
		'label'		=> esc_html__( 'Brand Sponsors', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopresss_disable_brands_sponsors', 
		'label'		=> esc_html__( 'DISABLE brand sponsors', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_brand_title', 
		'label'		=> esc_html__( 'Section Title', 'tokopress' ),
		'description'=> esc_html__( 'Default:', 'tokopress' ).' '.esc_html__( 'Our Sponsors', 'tokopress' ),
		'section'	=> 'tokopress_options_home_templates',
	);

	for ($i=1; $i <= 8; $i++) { 
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'image',
			'setting'	=> "tokopress_brand_img_{$i}",
			'label'		=> sprintf( esc_html__( 'Sponsor Logo #%s', 'tokopress' ), $i ),
			'section'	=> 'tokopress_options_home_templates',
		);
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'text',
			'default'	=> '#',
			'setting'	=> "tokopress_brand_link_{$i}",
			'label'		=> sprintf( esc_html__( 'Sponsor Link #%s', 'tokopress' ), $i ),
			'section'	=> 'tokopress_options_home_templates',
		);
	}

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_options_home_templates_goto_colors', 
		'label'		=> esc_html__( 'More ...', 'tokopress' ),
		'description' => '<p><span class="dashicons dashicons-admin-appearance"></span> <a href="javascript:wp.customize.section( \'tokopress_colors_home_templates\' ).focus();">'.esc_html__( 'Go to Theme Colors of this section', 'tokopress' ).'</a></p>',
		'section'	=> 'tokopress_options_home_templates',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_pagetemplates_contact' );
function tokopress_customize_controls_options_pagetemplates_contact( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_contact_templates',
		'title'    => esc_html__( 'Contact Page Template', 'tokopress' ),
		'panel'    => 'tokopress_pagetemplates',
		'priority' => 10,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'warning',
		'setting'	=> 'tokopress_contact_warning', 
		'label'		=> sprintf( esc_html__( 'You are not in %s', 'tokopress' ), esc_html__( 'Page with Contact Page Template', 'tokopress' ) ),
		'description' => sprintf( esc_html__( 'These settings only affect %s. Please visit %s to see live preview for these settings.', 'tokopress' ), esc_html__( 'Page with Contact Page Template', 'tokopress' ) , esc_html__( 'Page with Contact Page Template', 'tokopress' ) ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_is_not_contact_page',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_contact_heading_desc', 
		'label'		=> '',
		'description' => esc_html__( 'These options are for everyone who use Contact Page Template. We use simple contact form here. If you need advanced contact form (with more fields and setting), we recommend you to use Contact Form 7, Caldera Forms, Ninja Forms, WP Forms, or Gravity Forms plugin.', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_contact_title', 
		'label'		=> esc_html__( 'DISABLE Page Title in Contact page template', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_contact_sidebar', 
		'label'		=> esc_html__( 'DISABLE Sidebar in Contact page template', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_contact_heading_form', 
		'label'		=> esc_html__( 'Contact Form', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_contact_form', 
		'label'		=> esc_html__( 'DISABLE Contact Form', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '',
		'setting'	=> 'tokopress_contact_form_title',
		'label'		=> esc_html__( 'Contact Form - Title', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'input_attrs' => array(
			'placeholder' => __( 'Leave a Message', 'tokopress' ),
		),
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_contact_form_sendcopy_hide', 
		'label'		=> esc_html__( 'DISABLE "send copy" checkbox', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '',
		'setting'	=> 'tokopress_contact_form_button',
		'label'		=> esc_html__( 'Contact Form - Button Text', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'input_attrs' => array(
			'placeholder' => __( 'Submit', 'tokopress' ),
		),
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '',
		'setting'	=> 'tokopress_contact_form_email',
		'label'		=> esc_html__( 'Contact Form - Email Send To', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'input_attrs' => array(
			'placeholder' => get_bloginfo('admin_email'),
		),
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '',
		'setting'	=> 'tokopress_contact_form_subject',
		'label'		=> esc_html__( 'Contact Form - Email Subject', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'input_attrs' => array(
			'placeholder' => __( 'Message via the contact form', 'tokopress' ),
		),
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'heading',
		'setting'	=> 'tokopress_contact_heading_map', 
		'label'		=> esc_html__( 'Contact Map', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_contact_map', 
		'label'		=> esc_html__( 'DISABLE Google Map in Contact page template', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'radio',
		'default'   => 'api',
		'setting'	=> 'tokopress_contact_map_type', 
		'label'		=> esc_html__( 'Google Map Type', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'choices' 	=> array(
				'api' => esc_html__( 'Google Maps Embed API', 'tokopress' )."\n ".esc_html__( '(require API key)', 'tokopress' ),
				'embed' => esc_html__( 'Embed Map Using Address', 'tokopress' )."\n ".esc_html__( '(without API key)', 'tokopress' ),
				'iframe' => esc_html__( 'Embed Map Using Iframe', 'tokopress' )."\n ".esc_html__( '(without API key)', 'tokopress' ),
			),
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'setting'	=> 'tokopress_contact_apikey',
		'label'		=> esc_html__( 'Google Maps API Key (required)', 'tokopress' ),
		'description' => '<a href="https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key">'.esc_html__( 'Click here to get your Google Maps API key', 'tokopress' ).'</a>',
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_api',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '-6.903932',
		'setting'	=> 'tokopress_contact_lat',
		'label'		=> esc_html__( 'Latitude', 'tokopress' ),
		'description' => esc_html__( 'Insert Latitude coordinate', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_api',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> '107.610344',
		'setting'	=> 'tokopress_contact_long',
		'label'		=> esc_html__( 'Longitude', 'tokopress' ),
		'description' => esc_html__( 'Insert Longitude coordinate', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_api',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> esc_html__( 'Marker Title', 'tokopress' ),
		'setting'	=> 'tokopress_contact_marker_title',
		'label'		=> esc_html__( 'Marker Title', 'tokopress' ),
		'description' => esc_html__( 'Insert marker title', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_api',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> esc_html__( 'Marker Description', 'tokopress' ),
		'setting'	=> 'tokopress_contact_marker_desc',
		'label'		=> esc_html__( 'Marker Description', 'tokopress' ),
		'description' => esc_html__( 'Insert marker description', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_api',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'text',
		'default'	=> 'Gedung Sate, Bandung, Indonesia',
		'setting'	=> 'tokopress_contact_address',
		'label'		=> esc_html__( 'Address', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_embed',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'textarea-unfiltered',
		'default'	=> '',
		'setting'	=> 'tokopress_contact_iframe',
		'label'		=> esc_html__( 'Google Map Iframe Code', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_iframe',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'number',
		'default'	=> '15',
		'setting'	=> 'tokopress_contact_map_zoom',
		'label'		=> esc_html__( 'Zoom (1-20)', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
		'active_callback' => 'tokopress_callback_contact_map_zoom',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'number',
		'default'	=> '500',
		'setting'	=> 'tokopress_contact_map_height',
		'label'		=> esc_html__( 'Height (px)', 'tokopress' ),
		'section'	=> 'tokopress_options_contact_templates',
	);

	return $controls;
}

add_filter( 'tokopress_customize_controls', 'tokopress_customize_controls_options_optimization' );
function tokopress_customize_controls_options_optimization( $controls ) {

	$controls[] = array(
		'setting_type' => 'option_mod',
		'type'     => 'section',
		'setting'  => 'tokopress_options_optimization',
		'title'    => esc_html__( 'Optimizations', 'tokopress' ),
		'panel'    => 'tokopress_options',
		'priority' => 90,
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_wordpress_generator',
		'label'		=> esc_html__( 'DISABLE generator meta tag', 'tokopress' ),
		'description' => esc_html__( 'DISABLE generator meta tag from WordPress, WooCommerce, Visual Composer, and Revolution Slider for security purpose.', 'tokopress' ),
		'section'	=> 'tokopress_options_optimization',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_wordpress_emoji',
		'label'		=> esc_html__( 'DISABLE WordPress Emoji', 'tokopress' ),
		'description' => esc_html__( 'DISABLE WordPress Emoji script if you do not need it.', 'tokopress' ),
		'section'	=> 'tokopress_options_optimization',
	);

	$controls[] = array( 
		'setting_type' => 'option_mod',
		'type' 		=> 'checkbox',
		'setting'	=> 'tokopress_disable_wordpress_responsive_images',
		'label'		=> esc_html__( 'DISABLE WordPress Responsive Images', 'tokopress' ),
		'description' => esc_html__( 'DISABLE WordPress Responsive Images srcset if you do not need it.', 'tokopress' ),
		'section'	=> 'tokopress_options_optimization',
	);

	if ( is_child_theme() ) {
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_disable_child_theme_style',
			'label'		=> esc_html__( 'DISABLE Child Theme\'s style.css', 'tokopress' ),
			'description' => esc_html__( 'DISABLE Child Theme\'s style.css if you do not use it.', 'tokopress' ),
			'section'	=> 'tokopress_options_optimization',
		);
	}

	if ( class_exists( 'Jetpack' ) ) {
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_disable_child_theme_style',
			'label'		=> esc_html__( 'DISABLE Jetpack JS & CSS', 'tokopress' ),
			'description' => esc_html__( 'DISABLE Jetpack JS & CSS if you do not need it.', 'tokopress' ),
			'section'	=> 'tokopress_options_optimization',
		);
	}

	if ( class_exists( 'woocommerce' ) ) {
		$controls[] = array( 
			'setting_type' => 'option_mod',
			'type' 		=> 'checkbox',
			'setting'	=> 'tokopress_custom_woocommerce_smallscreen',
			'label'		=> esc_html__( 'Custom WooCommerce Small Screen CSS', 'tokopress' ),
			'description' => esc_html__( 'Load custom woocommerce small screen CSS file. It is useful when your CSS minify plugin does not work.', 'tokopress' ),
			'section'	=> 'tokopress_options_optimization',
		);
	}

	return $controls;
}

add_action( 'admin_menu', 'tokopress_customize_theme_page_menu', 1 );
function tokopress_customize_theme_page_menu() {
	add_theme_page( esc_html__( 'Theme Options', 'tokopress' ), esc_html__( 'Theme Options', 'tokopress' ), 'edit_theme_options', 'customize.php' );
}

function tokopress_callback_is_not_home_page() {
	return is_page_template( 'page_home_event.php' ) ? false : true;
}

function tokopress_callback_is_not_contact_page() {
	return is_page_template( 'page_contact.php' ) ? false : true;
}

function tokopress_callback_home_event_slider_not_by_ids() {
	$home_slider_ids = tokopress_get_mod( 'tokopress_home_slider_ids' );
	return empty( $home_slider_ids ) ? true : false;
}

function tokopress_callback_contact_map_api() {
	$map_type = tokopress_get_mod( 'tokopress_contact_map_type' );
	return ! $map_type || $map_type == 'api' ? true : false;
}

function tokopress_callback_contact_map_embed() {
	$map_type = tokopress_get_mod( 'tokopress_contact_map_type' );
	return $map_type == 'embed' ? true : false;
}

function tokopress_callback_contact_map_iframe() {
	$map_type = tokopress_get_mod( 'tokopress_contact_map_type' );
	return $map_type == 'iframe' ? true : false;
}

function tokopress_callback_contact_map_zoom() {
	$map_type = tokopress_get_mod( 'tokopress_contact_map_type' );
	return ! $map_type || $map_type == 'api' || $map_type == 'embed' ? true : false;
}

add_action( 'customize_controls_print_scripts', 'tokopress_customize_print_scripts', 30 );
function tokopress_customize_print_scripts() {
	$page_ids = get_posts( array(
		'posts_per_page' => 1,
		'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page_home_event.php',
		'fields' => 'ids',
	) );
	$page_home = !empty( $page_ids ) ? get_permalink( reset( $page_ids ) ) : '';
	$page_ids = get_posts( array(
		'posts_per_page' => 1,
		'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page_contact.php',
		'fields' => 'ids',
	) );
	$page_contact = !empty( $page_ids ) ? get_permalink( reset( $page_ids ) ) : '';
?>
<script type="text/javascript">
jQuery( document ).ready( function( $ ) {
<?php if ( $page_home ) : ?>
	wp.customize.section( 'tokopress_options_home_templates', function( section ) {
		section.expanded.bind( function( isExpanded ) {
			if ( isExpanded ) {
				wp.customize.previewer.previewUrl.set( '<?php echo esc_js( $page_home ); ?>' );
			}
		} );
	} );
	wp.customize.section( 'tokopress_colors_home_templates', function( section ) {
		section.expanded.bind( function( isExpanded ) {
			if ( isExpanded ) {
				wp.customize.previewer.previewUrl.set( '<?php echo esc_js( $page_home ); ?>' );
			}
		} );
	} );
<?php endif; ?>
<?php if ( $page_contact ) : ?>
	wp.customize.section( 'tokopress_options_contact_templates', function( section ) {
		section.expanded.bind( function( isExpanded ) {
			if ( isExpanded ) {
				wp.customize.previewer.previewUrl.set( '<?php echo esc_js( $page_contact ); ?>' );
			}
		} );
	} );
<?php endif; ?>
} );
</script>
<?php
}
