<?php

add_theme_support( 'woocommerce' );

/**
 * Redirect Option After customer Login
 */
function tokopress_wc_login_redirect( $redirect_to ) {
    $redirect_to = esc_url( tokopress_get_mod( 'tokopress_wc_red_cus_login' ) );
    return $redirect_to;
}
if( tokopress_get_mod( 'tokopress_wc_red_cus_login' ) ) {
	add_filter( 'woocommerce_login_redirect', 'tokopress_wc_login_redirect' );
}