<?php 
global $tp_sidebar, $tp_content_class, $tp_post_classes;
if ( tokopress_get_mod('tokopress_post_hide_sidebar') ) {
	$tp_sidebar = false;
	$tp_content_class = 'col-md-12';
}
else {
	$tp_sidebar = true;
	$tp_content_class = 'col-md-9';
}
$tp_post_meta = tokopress_get_mod('tokopress_post_meta_position');
if ( 'bottom' == $tp_post_meta ) {
	$tp_post_content_class = 'col-md-12';
	$tp_post_meta_class = 'col-md-12';
}
elseif ( 'hide' == $tp_post_meta ) {
	$tp_post_content_class = 'col-md-12';
	$tp_post_meta_class = 'col-md-12';
}
else {
	$tp_post_content_class = 'col-md-8 col-md-push-4';
	$tp_post_meta_class = 'col-md-4 col-md-pull-8';
}
get_header(); 
?>

	<?php if ( ! tokopress_get_mod( 'tokopress_page_title_disable' ) ) : ?>
		<?php get_template_part( 'block-page-title' ); ?>
	<?php endif; ?>

	<div id="main-content">
		
		<div class="container">
			<div class="row">
				
				<div class="<?php echo esc_attr( $tp_content_class ); ?>">
					
					<?php if ( have_posts() ) : ?>

						<div class="main-wrapper">

							<?php do_action( 'tokopress_before_content' ); ?>
				
							<?php while ( have_posts() ) : the_post(); ?>
								
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'blog-single clearfix' ); ?>>
	
									<div class="inner-post">
										<?php if ( ! tokopress_get_mod( 'tokopress_post_hide_image' ) ) : ?>
											<?php if( has_post_thumbnail() ) : ?>
												<div class="post-thumbnail">
													<?php the_post_thumbnail(); ?>
												</div>
											<?php endif; ?>
										<?php endif; ?>

										<div class="row">

											<div class="<?php echo esc_attr( $tp_post_content_class ); ?>">
												<div class="post-summary">
													<?php if( ! tokopress_get_mod( 'tokopress_page_title_disable' ) ) : ?>
													    <h2 class="entry-title post-title screen-reader-text"><?php the_title(); ?></h2>
													<?php else : ?>
													    <h1 class="entry-title post-title"><?php the_title(); ?></h1>
													<?php endif; ?>

													<div class="entry-content">
														<?php the_content(); ?>
													</div>

													<?php wp_link_pages( array( 'before' => '<p class="page-link"><span>' . __( 'Pages:', 'tokopress' ) . '</span>', 'after' => '</p>' ) ); ?>
												</div>
											</div>

											<?php if ( 'hide' != $tp_post_meta ) : ?>
											<div class="<?php echo esc_attr( $tp_post_meta_class ); ?>">
												<div class="post-meta">
													<ul class="list-post-meta">
														<li>
															<div class="post-date">
																<?php echo tokopress_get_post_date(); ?>
															</div>
														</li>
														<li>
															<div class="post-term-category"><?php echo tokopress_get_post_terms( array( 'taxonomy' => 'category', 'before' => '<p>' . __( 'Posted Under', 'tokopress' ) . '</p>' ) ); ?></div>
														</li>
														<li>
															<?php echo tokopress_get_post_author(); ?>
														</li>

														<?php echo tokopress_get_post_terms( array( 'before' => '<li><div class="post-term-tags"><p>' . __( 'Tags', 'tokopress' ) . '</p></div></li>' ) ); ?>
		
													</ul>
												</div>
											</div>
											<?php endif; ?>

										</div>
									</div>

								</article>

							<?php endwhile; ?>

							<?php
			                if ( comments_open() || '0' != get_comments_number() ) :
			                    comments_template();
			                endif;
			                ?>

							<?php do_action( 'tokopress_after_content' ); ?>

						</div>
						
					<?php else : ?>

						<?php get_template_part( 'content', '404' ); ?>

					<?php endif; ?>

				</div>

				<?php if ( $tp_sidebar ) get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>