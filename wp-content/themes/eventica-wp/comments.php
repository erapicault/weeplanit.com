<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$title = tokopress_get_mod( 'tokopress_post_comments_custom_title' );
if ( !trim($title) ) {
	$title = esc_html__( 'Comments', 'tokopress' );
} 

?>

<?php if ( post_password_required() ) : ?>
	<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'tokopress' ); ?></p>
	<?php return; ?>
<?php endif; ?>
	
<?php if ( comments_open() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

	<?php // You can start editing here -- including this comment! ?>

    <section id="comments" class="section-wrap comments-wrap clearfix">
		<div class="section-title comments-title">
			<h2 class="section-heading"><?php echo esc_html( $title ); ?></h2>
		</div>

		<div class="section-area comments-area">

			<?php if ( have_comments() ) : ?>
				<div class="commentslist-wrap">
					<ol class="commentlist">
						<?php wp_list_comments( 'callback=tokopress_comments' ); ?>
					</ol>
					<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
					<nav id="comment-nav-below">
						<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'tokopress' ) ); ?></div>
						<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'tokopress' ) ); ?></div>
					</nav>
					<?php endif; // check for comment navigation ?>
				</div>
			<?php endif; ?>

			<?php
		    $comment_args = array(
		        'title_reply'           => esc_html__( 'Leave a comment', 'tokopress' ),
		        'fields'                => apply_filters( 
		                                    'comment_form_default_fields',
		                                    array(
		                                        'block-open'    => '',
		                                        'author'        => '<p><input id="input_name" class="input-text" name="author" type="text" placeholder="' . __( 'Name *', 'tokopress' ) . '" /></p>',   
		                                        'email'         => '<p><input id="input_email" class="input-text" name="email" type="email" placeholder="' . __( 'Email *', 'tokopress' ) . '" /></p>',
		                                        'url'           => '<p><input id="input_url" class="input-text" name="url" type="text" placeholder="' . __( 'Website', 'tokopress' ) . '" /></p>',
		                                        'block-close'   => ''
		                                    )
		                                ),
		        'comment_field'         => '<p><textarea id="comment_message" class="input-text" rows="10" name="comment" placeholder="' . __( 'post your comment here! *', 'tokopress' ) . '" aria-required="true"></textarea></p>',
		        'comment_notes_after'   => '',
		        'label_submit'          => __( 'Post comment', 'tokopress' )
		    );
		    comment_form( $comment_args ); 
		    ?>
		</div>
	</section><!-- #comments -->
<?php endif; ?>
