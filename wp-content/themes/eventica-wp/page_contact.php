<?php
/**
 * Template Name: Contact
 * Description: The template for displaying Contact Form page. 
 *
 * WARNING: This file is part of the Eventica parent theme.
 * Please do all modifications in the form of a child theme.
 *
 * @category Page
 * @package  Templates
 * @author   TokoPress
 * @link     http://www.tokopress.com
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $tp_sidebar, $tp_content_class;

if( "" != tokopress_get_mod( 'tokopress_disable_contact_sidebar' ) ) {
	$tp_sidebar = false;
	$tp_content_class = 'col-md-12';
}
else {
	$tp_sidebar = true;
	$tp_content_class = 'col-md-9';
}

$map_type = tokopress_get_mod( 'tokopress_contact_map_type' );
$map_apikey = tokopress_get_mod( 'tokopress_contact_apikey' );

$latitude = tokopress_get_mod( 'tokopress_contact_lat' );
if ( !$latitude ) {
	$latitude = -6.903932;
}

$longitude = tokopress_get_mod( 'tokopress_contact_long' );
if ( !$longitude ) {
	$longitude = 107.610344;
}

$marker_title = tokopress_get_mod( 'tokopress_contact_marker_title' );
if ( !$marker_title ) {
	$marker_title = esc_html__( 'Marker Title', 'tokopress' );
}
$marker_title = str_replace( "\r\n", "<br/>", $marker_title );

$marker_content = tokopress_get_mod( 'tokopress_contact_marker_desc' );
if ( !$marker_content ) {
	$marker_content = esc_html__( 'Marker Content', 'tokopress' );
}
$marker_content = str_replace( "\r\n", "<br/>", $marker_content );

$map_zoom = tokopress_get_mod( 'tokopress_contact_map_zoom' );
if ( !$map_zoom ) {
	$map_zoom = 15;
}

$map_height = tokopress_get_mod( 'tokopress_contact_map_height' );
if ( !$map_height ) {
	$map_height = 500;
}

$map_address = tokopress_get_mod( 'tokopress_contact_address' );
if ( !$map_address ) {
	$map_address = 'Gedung Sate, Bandung, Indonesia';
}

?>

<?php get_header(); ?>

	<?php if( ! tokopress_get_mod( 'tokopress_page_title_disable' ) ) : ?>
		<?php if( "" == tokopress_get_mod( 'tokopress_disable_contact_title' ) ) get_template_part( 'block-page-title' ); ?>
	<?php endif; ?>

	<div id="main-content">
		
		<div class="container">
			<div class="row">
				
				<div class="<?php echo esc_attr( $tp_content_class ); ?>">
					
					<?php if ( have_posts() ) : ?>

						<div class="main-wrapper">

							<?php do_action( 'tokopress_before_content' ); ?>

							<?php if( !tokopress_get_mod( 'tokopress_disable_contact_map' ) ) : ?>

								<?php if( ( !$map_type || 'api' == $map_type ) && $map_apikey )  : ?>

									<script type="text/javascript">
										var map;
										var map_latitude = <?php echo esc_js( $latitude ); ?>;
										var map_longitude = <?php echo esc_js( $longitude ); ?>;
										var markerTitle = "<?php echo esc_js( $marker_title ); ?>";
										var markerContent = "<h1><?php echo esc_js( $marker_title ); ?></h1><p><?php echo esc_js( $marker_content ); ?></p>";

										jQuery(document).ready(function(){map=new GMaps({el:"#map",lat:map_latitude,lng:map_longitude,zoom:<?php echo intval($map_zoom); ?>,scrollwheel:false});map.addMarker({lat:map_latitude,lng:map_longitude,title:markerTitle,infoWindow:{content:markerContent}})})
									</script>

									<div id="map" style="background-color:rgb(229,227,223);height:<?php echo intval($map_height); ?>px;"></div>

								<?php elseif ( 'iframe' == $map_type ) : ?>

									<div id="map" style="background-color:rgb(229,227,223);height:<?php echo intval($map_height); ?>px;">
										<?php 
										$map_iframe = tokopress_get_mod( 'tokopress_contact_iframe' );
										if ( $map_iframe ) {
											preg_match_all('/<iframe[^>]+src="([^"]+)"/', $map_iframe, $map_iframe_match);
											if ( isset( $map_iframe_match[1][0] ) && $map_iframe_match[1][0] ) {
												?>
												<iframe width="100%" height="<?php echo intval($map_height); ?>" src="<?php echo esc_url( $map_iframe_match[1][0] ); ?>" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
												<?php
											}
										}
										?>
									</div>

								<?php else : ?>

									<?php if ( 'embed' != $map_type ) $map_address = '-6.903932,107.610344'; ?>
									<div id="map" style="background-color:rgb(229,227,223);height:<?php echo intval($map_height); ?>px;">
										<iframe width="100%" height="<?php echo intval($map_height); ?>" src="https://maps.google.com/maps?width=100%&amp;height=<?php echo intval($map_height); ?>&amp;q=<?php echo rawurlencode( trim( $map_address) ); ?>&amp;ie=UTF8&amp;t=&amp;z=<?php echo intval($map_zoom); ?>&amp;iwloc=near&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
									</div>

								<?php endif; ?>

								<?php $contact_class = 'page-contact no-margin'; ?>

							<?php else: ?>
								<?php $contact_class = 'page-contact'; ?>
							<?php endif; ?>
				
							<?php while ( have_posts() ) : the_post(); ?>
								
								<article id="page-<?php the_ID(); ?>" <?php post_class( $contact_class . ' clearfix' ); ?>>
	
									<div class="inner-page">
										<?php if( has_post_thumbnail() ) : ?>
											<div class="post-thumbnail">
												<?php the_post_thumbnail(); ?>
											</div>
										<?php endif; ?>

										<div class="col-md-12">
											<div class="post-summary">
												<?php if( ! tokopress_get_mod( 'tokopress_page_title_disable' ) ) : ?>
												    <h2 class="post-title screen-reader-text"><?php the_title(); ?></h2>
												<?php else : ?>
												    <h1 class="post-title"><?php the_title(); ?></h2>
												<?php endif; ?>

												<?php the_content(); ?>

												<?php wp_link_pages( array( 'before' => '<p class="page-link"><span>' . __( 'Pages:', 'tokopress' ) . '</span>', 'after' => '</p>' ) ); ?>
											</div>
										</div>
									</div>

								</article>

							<?php endwhile; ?>

							<?php
							if ( ! tokopress_get_mod( 'tokopress_disable_contact_form' ) ) {

								$args = array();

								$title = tokopress_get_mod( 'tokopress_contact_form_title' );
								if ( $title ) $args['title'] = $title;

								$sendcopy = tokopress_get_mod( 'tokopress_contact_form_sendcopy_hide' );
								if ( $sendcopy ) $args['sendcopy'] = 'no';

								$button_text = tokopress_get_mod( 'tokopress_contact_form_button' );
								if ( $button_text ) $args['button_text'] = $button_text;

								$email = tokopress_get_mod( 'tokopress_contact_form_email' );
								if ( $email ) $args['email'] = $email;

								$subject = tokopress_get_mod( 'tokopress_contact_form_subject' );
								if ( $subject ) $args['subject'] = $subject;

								echo tokopress_get_contact_form( $args );

							}
							?>

							<?php do_action( 'tokopress_after_content' ); ?>

						</div>
						
					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>

				</div>

				<?php if ( $tp_sidebar ) get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>