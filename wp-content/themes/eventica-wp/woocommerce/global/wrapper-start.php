<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php do_action( 'tokopress_wc_before_wrapper' ); ?>

<div id="main-content">
		
	<div class="container">
		<div class="row">

		<?php do_action( 'tokopress_wc_before_content' ); ?>