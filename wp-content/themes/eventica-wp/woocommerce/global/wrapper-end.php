<?php
/**
 * Content wrappers
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

		<?php do_action( 'tokopress_wc_after_content' ); ?>

		</div>
	</div>

</div>

<?php do_action( 'tokopress_wc_after_wrapper' ); ?>