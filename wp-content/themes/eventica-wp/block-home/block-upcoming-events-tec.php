<?php if( ! class_exists( 'Tribe__Events__Main' ) ) : ?>
	<p class="nothing-plugins"><?php _e( 'Please install <strong>The Event Calendar</strong> plugin.', 'tokopress' ); ?></p>
	<?php return; ?>
<?php endif; ?>

<?php

$numbers = intval( tokopress_get_mod( 'tokopress_home_upcoming_event_numbers' ) );
if ( $numbers < 1 )
	$numbers = 3;

$args = array(
	'post_type'		 => array(Tribe__Events__Main::POSTTYPE),
	'posts_per_page' => $numbers,
	'orderby'        => 'event_date',
	'order'          => 'ASC',
	//required in 3.x
	'eventDisplay'=>'list'
);

if ( $category = tokopress_get_mod('tokopress_home_upcoming_event_category') ) {
	$cat_ids_include = array();
	$cat_name_include = explode( ',', $category );
	foreach ( $cat_name_include as $cat_name ) {
		$term = term_exists( $cat_name, 'tribe_events_cat' );
		if ($term !== 0 && $term !== null) {
			$cat_ids_include[] = $term['term_id'];
		}
	}
	if ( ! empty( $cat_ids_include ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy'	=> 'tribe_events_cat',
				'terms'   	=> $cat_ids_include,
				'operator'	=> 'IN'
			)
		);
	}
	else {
		if ( count( $cat_name_include ) > 1 ) {
			return '<p>' . sprintf( esc_html__( 'Event categories (%s) do not exist.', 'tokopress' ), $category ) . '</p>';
		}
		else {
			return '<p>' . sprintf( esc_html__( 'Event category (%s) does not exist.', 'tokopress' ), $category ) . '</p>';
		}
	}
}
if ( $category_exclude = tokopress_get_mod('tokopress_home_upcoming_event_category_exclude') ) {
	$cat_ids_exclude = array();
	$cat_name_exclude = explode( ',', $category_exclude );
	foreach ( $cat_name_exclude as $cat_name ) {
		$term = term_exists( $cat_name, 'tribe_events_cat' );
		if ($term !== 0 && $term !== null) {
			$cat_ids_exclude[] = $term['term_id'];
		}
	}
	if ( ! empty( $cat_ids_exclude ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'tribe_events_cat',
			'terms'   	=> $cat_ids_exclude,
			'operator'	=> 'NOT IN',
		);
	}
}

if ( $tag = tokopress_get_mod('tokopress_home_upcoming_event_tag') ) {
	$tag_ids_include = array();
	$tag_name_include = explode( ',', $tag );
	foreach ( $tag_name_include as $tag_name ) {
		$term = term_exists( $tag_name, 'post_tag' );
		if ($term !== 0 && $term !== null) {
			$tag_ids_include[] = $term['term_id'];
		}
	}
	if ( ! empty( $tag_ids_include ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'post_tag',
			'terms'   	=> $tag_ids_include,
			'operator'	=> 'IN',
		);
	}
	else {
		if ( count( $tag_name_include ) > 1 ) {
			return '<p>' . sprintf( esc_html__( 'Event tag (%s) do not exist.', 'tokopress' ), $category ) . '</p>';
		}
		else {
			return '<p>' . sprintf( esc_html__( 'Event tag (%s) does not exist.', 'tokopress' ), $category ) . '</p>';
		}
	}
}
if ( $tag_exclude = tokopress_get_mod('tokopress_home_upcoming_event_tag_exclude') ) {
	$tag_ids_exclude = array();
	$tag_name_exclude = explode( ',', $tag_exclude );
	foreach ( $tag_name_exclude as $tag_name ) {
		$term = term_exists( $tag_name, 'post_tag' );
		if ($term !== 0 && $term !== null) {
			$tag_ids_exclude[] = $term['term_id'];
		}
	}
	if ( ! empty( $tag_ids_exclude ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'post_tag',
			'terms'   	=> $tag_ids_exclude,
			'operator'	=> 'NOT IN',
		);
	}
}

$ids = trim( tokopress_get_mod( 'tokopress_home_upcoming_event_exclude' ) );
if ( ! empty( $ids ) ) {
	$ids = explode( ",", $ids );
	$args['post__not_in'] = $ids;
}

$the_upcoming_events = new WP_Query( $args );
?>

<?php if( $the_upcoming_events->have_posts() ) : ?>
<div class="home-upcoming-events clearfix">
	<div class="container">

		<a class="upcoming-event-nav" href="<?php echo tribe_get_events_link() ?>">
			<?php if( "" != tokopress_get_mod( 'tokopress_home_upcoming_event_text' ) ) : ?>
				<?php echo esc_attr( tokopress_get_mod( 'tokopress_home_upcoming_event_text' ) ); ?> 
			<?php else : ?>
				<?php _e( 'All Events', 'tokopress' ); ?> 
			<?php endif; ?>
			<i class="fa fa-chevron-right"></i>
		</a>

		<h2 class="upcoming-event-title">
			<?php if( "" != tokopress_get_mod( 'tokopress_home_upcoming_event' ) ) : ?>
				<?php echo esc_attr( tokopress_get_mod( 'tokopress_home_upcoming_event' ) ); ?>
			<?php else : ?>
				<?php _e( 'Upcoming Events', 'tokopress' ); ?>
			<?php endif; ?>
		</h2>

		<div class="row">

			<div class="upcoming-event-wrap tribe-events-list">

				<div class="events-loop">
					<?php while ( $the_upcoming_events->have_posts() ) : ?>
						<?php $the_upcoming_events->the_post(); ?>
						
						<div id="post-<?php the_ID() ?>" class="<?php tribe_events_event_classes() ?> col-sm-12 col-md-12">
							<?php tribe_get_template_part( 'list/single', 'event' ); ?>
						</div>
								
					<?php endwhile; ?>
				</div>

			</div>

		</div>
	</div>

</div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>