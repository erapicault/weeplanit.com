<?php

$numbers = intval( tokopress_get_mod( 'tokopress_home_recent_post_numbers' ) );
if ( $numbers < 1 )
	$numbers = 3;

$args = array(
	'post_status'=>'publish',
	'post_type'=> 'post',
	'posts_per_page'=> $numbers,
	'orderby'=>'date',
	'order'=>'DESC',
	'ignore_sticky_posts' => true
	);


if ( $category = tokopress_get_mod('tokopress_home_recent_post_category') ) {
	$cat_ids_include = array();
	$cat_name_include = explode( ',', $category );
	foreach ( $cat_name_include as $cat_name ) {
		$term = term_exists( $cat_name, 'category' );
		if ($term !== 0 && $term !== null) {
			$cat_ids_include[] = $term['term_id'];
		}
	}
	if ( ! empty( $cat_ids_include ) ) {
		$args['tax_query'] = array(
			array(
				'taxonomy'	=> 'category',
				'terms'   	=> $cat_ids_include,
				'operator'	=> 'IN'
			)
		);
	}
	else {
		if ( count( $cat_name_include ) > 1 ) {
			return '<p>' . sprintf( esc_html__( 'Post categories (%s) do not exist.', 'tokopress' ), $category ) . '</p>';
		}
		else {
			return '<p>' . sprintf( esc_html__( 'Post category (%s) does not exist.', 'tokopress' ), $category ) . '</p>';
		}
	}
}
if ( $category_exclude = tokopress_get_mod('tokopress_home_recent_post_category_exclude') ) {
	$cat_ids_exclude = array();
	$cat_name_exclude = explode( ',', $category_exclude );
	foreach ( $cat_name_exclude as $cat_name ) {
		$term = term_exists( $cat_name, 'category' );
		if ($term !== 0 && $term !== null) {
			$cat_ids_exclude[] = $term['term_id'];
		}
	}
	if ( ! empty( $cat_ids_exclude ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'category',
			'terms'   	=> $cat_ids_exclude,
			'operator'	=> 'NOT IN',
		);
	}
}

if ( $tag = tokopress_get_mod('tokopress_home_recent_post_tag') ) {
	$tag_ids_include = array();
	$tag_name_include = explode( ',', $tag );
	foreach ( $tag_name_include as $tag_name ) {
		$term = term_exists( $tag_name, 'post_tag' );
		if ($term !== 0 && $term !== null) {
			$tag_ids_include[] = $term['term_id'];
		}
	}
	if ( ! empty( $tag_ids_include ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'post_tag',
			'terms'   	=> $tag_ids_include,
			'operator'	=> 'IN',
		);
	}
	else {
		if ( count( $tag_name_include ) > 1 ) {
			return '<p>' . sprintf( esc_html__( 'Post tag (%s) do not exist.', 'tokopress' ), $category ) . '</p>';
		}
		else {
			return '<p>' . sprintf( esc_html__( 'Post tag (%s) does not exist.', 'tokopress' ), $category ) . '</p>';
		}
	}
}
if ( $tag_exclude = tokopress_get_mod('tokopress_home_recent_post_tag_exclude') ) {
	$tag_ids_exclude = array();
	$tag_name_exclude = explode( ',', $tag_exclude );
	foreach ( $tag_name_exclude as $tag_name ) {
		$term = term_exists( $tag_name, 'post_tag' );
		if ($term !== 0 && $term !== null) {
			$tag_ids_exclude[] = $term['term_id'];
		}
	}
	if ( ! empty( $tag_ids_exclude ) ) {
		$args['tax_query'][] =array(
			'taxonomy'	=> 'post_tag',
			'terms'   	=> $tag_ids_exclude,
			'operator'	=> 'NOT IN',
		);
	}
}

$ids = trim( tokopress_get_mod( 'tokopress_home_recent_post_exclude' ) );
if ( ! empty( $ids ) ) {
	$ids = explode( ",", $ids );
	$args['post__not_in'] = $ids;
}

$the_recent_post = new WP_Query( $args );
?>

<?php if( $the_recent_post->have_posts() ) : ?>
<div class="home-recent-posts">

	<div class="recent-post-wrap">

		<a class="recent-post-nav" href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">
			<?php if( "" != tokopress_get_mod( 'tokopress_home_recent_post_text' ) ) : ?>
				<?php echo esc_attr( tokopress_get_mod( 'tokopress_home_recent_post_text' ) ); ?> 
			<?php else : ?>
				<?php _e( 'All Posts', 'tokopress' ); ?> 
			<?php endif; ?>
			<i class="fa fa-chevron-right"></i>
		</a>

		<h2 class="recent-post-title">
			<?php if( "" != tokopress_get_mod( 'tokopress_home_recent_post' ) ) : ?>
				<?php echo esc_attr( tokopress_get_mod( 'tokopress_home_recent_post' ) ); ?>
			<?php else : ?>
				<?php _e( 'Recent Updates', 'tokopress' ); ?>
			<?php endif; ?>
		</h2>

		<div class="row">
			<?php while ( $the_recent_post->have_posts() ) : ?>
				<?php $the_recent_post->the_post(); ?>

					<?php
					global $tp_post_classes;
					$tp_post_classes = 'col-sm-6 col-md-12';
					?>
					<?php get_template_part( 'content', get_post_format() ); ?>
						
			<?php endwhile; ?>
		</div>

	</div>

</div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>