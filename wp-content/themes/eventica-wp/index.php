<?php 
global $tp_sidebar, $tp_content_class, $tp_post_classes;
if ( tokopress_get_mod('tokopress_blog_hide_sidebar') ) {
	$tp_sidebar = false;
	$tp_content_class = 'col-md-12';
	$tp_post_classes = 'col-sm-6 col-md-4';
}
else {
	$tp_sidebar = true;
	$tp_content_class = 'col-md-9';
	$tp_post_classes = 'col-sm-6 col-md-6';
}
get_header(); 
?>

	<?php if( ! tokopress_get_mod( 'tokopress_page_title_disable' ) ) : ?>
		<?php get_template_part( 'block-page-title' ); ?>
	<?php endif; ?>

	<div id="main-content">
		
		<div class="container">
			<div class="row">
				
				<div class="<?php echo esc_attr( $tp_content_class ); ?>">
					
					<?php if ( have_posts() ) : ?>

						<div class="main-wrapper">

							<?php do_action( 'tokopress_before_content' ); ?>

							<div class="blog-wrapper clearfix">
							<div class="row">

							<?php
							/**
							 * Mod Class
							 */
							$counter = 1;
							?>
				
							<?php while ( have_posts() ) : the_post(); ?>

								<?php if( class_exists( 'Tribe__Events__Main' ) && get_post_type() == 'tribe_events' ) : ?>
									<div id="post-<?php the_ID() ?>" class="<?php tribe_events_event_classes() ?> blog-list tribe-events-list <?php echo esc_attr( $tp_post_classes ); ?>">
										<?php tribe_get_template_part( 'list/single', 'event' ) ?>
									</div>
								<?php else : ?>
									<?php get_template_part( 'content', get_post_format() ); ?>
								<?php endif; ?>

								<?php if ( $tp_sidebar ) : ?>
									<?php if ( 0 == $counter % 2 ) : ?>
										<div class="clearfix visible-sm visible-md visible-lg"></div>
									<?php endif; ?>
								<?php else : ?>
									<?php if ( 0 == $counter % 6 ) : ?>
										<div class="clearfix visible-sm visible-md visible-lg"></div>
									<?php elseif ( 0 == $counter % 3 ) : ?>
										<div class="clearfix visible-md visible-lg"></div>
									<?php elseif ( 0 == $counter % 2 ) : ?>
										<div class="clearfix visible-sm"></div>
									<?php endif; ?>
								<?php endif; ?>

							<?php $counter++; endwhile; ?>

							</div>
							</div>

							<?php do_action( 'tokopress_after_content' ); ?>

						</div>
						
					<?php else : ?>

						<?php get_template_part( 'content', 'none' ); ?>

					<?php endif; ?>

				</div>

				<?php if ( $tp_sidebar ) get_sidebar(); ?>

			</div>
		</div>
	</div>

<?php get_footer(); ?>