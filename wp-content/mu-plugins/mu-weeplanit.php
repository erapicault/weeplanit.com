<?php


add_filter('locale', 'weeplanit_locale', 10);
function weeplanit_locale($locale){
	if (strpos($_SERVER['REQUEST_URI'], '/fr/') !== false)
		return 'fr_FR';
	return $locale;
}

?>