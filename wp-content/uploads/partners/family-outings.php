<?php

function init_family_outings(){
	global $weeplanit_title;
	global $weeplanit_type;
	$category = get_queried_object();
	$category_parent_id = $category->parent;
	if ( $category_parent_id != 0 ) {
 		$category_parent = get_term_by('id', $category_parent_id, 'tribe_events_cat');
 		if($category_parent->slug == "family-outings"){
 			$weeplanit_title = term_description( $category_parent_id, 'tribe_events_cat' );
 			$weeplanit_type = "family-outings";
 			add_action( 'wp_enqueue_scripts', 'add_outing_stylesheet' );
 		}
 	}
}

function add_outing_stylesheet() { 
 		wp_enqueue_style('family-outings', '/wp-content/themes/eventica-wp-child/family-outings.css');
 }
?>