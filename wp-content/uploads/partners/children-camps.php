<?php

function init_children_camps(){
	global $weeplanit_title;
	global $weeplanit_type;
	$category = get_queried_object();
	$category_parent_id = $category->parent;
	if ( $category_parent_id != 0 ) {
 		$category_parent = get_term_by('id', $category_parent_id, 'tribe_events_cat');

 		if($category_parent->slug == "pd-day"){
 			$weeplanit_title = term_description( $category_parent_id, 'tribe_events_cat' );
 			$weeplanit_type = "children-camps";
 			add_action( 'wp_enqueue_scripts', 'add_camp_stylesheet' );
 		}
 	}
}

function add_camp_stylesheet() {    
 	global $weeplanit_type;
 	wp_enqueue_style('children-camps', '/wp-content/themes/eventica-wp-child/children-camps.css');
 }
?>