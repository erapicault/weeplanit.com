<?php
$weeplanit_language = substr(get_bloginfo( 'language' ), 0, 2);
include( get_stylesheet_directory() . "/partners.php");
include( get_stylesheet_directory() . "/children-camps.php");
include( get_stylesheet_directory() . "/filters.php");
include( get_stylesheet_directory() . "/family-outings.php");
include( get_stylesheet_directory() . "/school-activities.php");

$weeplanit_title;
$weeplanit_subtitle;
$weeplanit_logo;
$weeplanit_css;
$weeplanit_type;

add_action( 'pre_get_posts', 'weeplanit_init' );
function weeplanit_init(){
	global $weeplanit_title;
	global $weeplanit_logo;
	$weeplanit_logo = esc_url( of_get_option( 'tokopress_site_logo' ));
	global $weeplanit_subtitle;
	$weeplanit_subtitle = term_description( get_queried_object_id(), 'tribe_events_cat' );
	init_family_outings();
	init_partner();
	init_children_camps();
	init_school_activities();
} 	

add_filter( 'pre_get_document_title', 'weeplanit_title', 1, 1 );
function weeplanit_title( $title) {
	global $weeplanit_title;
	return wp_strip_all_tags($weeplanit_title);
}
/****************************************************************/
/* Auto-redirect vers la page de l'organisateur de l'évènement  */
/****************************************************************/
add_action( 'template_redirect', 'weeplanit_redirect' );
function weeplanit_redirect() {
  global $weeplanit_language;
  $queried_post_type = get_query_var('post_type');
  if (is_single() && 'tribe_events' ==  $queried_post_type ) {
    $post = get_post();
    $urlNew = get_post_meta($post->ID, '_EventURL', true);
    $urlOld = get_post_meta($post->ID, ($weeplanit_language == 'fr' ? '_ecp_custom_26' : "_ecp_custom_25"), true);
    $url = ($urlNew == "" ? $urlOld : $urlNew);
    if($url != null && $url != ""){
   	 	wp_redirect($url);
    	exit;
    }
  }
}

function fb_opengraph() {
    global $post;
 
    if(is_single() && tribe_is_event()) {
        if(has_post_thumbnail($post->ID)) {
            $img_src = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
        } else {
            $img_src = get_stylesheet_directory_uri() . '/img/opengraph_image.jpg';
        }
        if($excerpt = $post->post_excerpt) {
            $excerpt = strip_tags($post->post_excerpt);
            $excerpt = str_replace("", "'", $excerpt);
        } else {
            $excerpt = get_bloginfo('description');
        }
        ?>
 
    <meta property="og:title" content="<?php echo the_title(); ?>"/>
    <meta property="og:description" content="<?php echo $excerpt; ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
    <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
    <meta property="og:image" content="<?php echo the_post_thumbnail( 'medium' ) ?>"/>
 
<?php
    } else {
        return;
    }
}
add_action('wp_head', 'fb_opengraph', 5);


add_filter('excerpt_length', 'weeplanit_excerpt_length',10,1);
function weeplanit_excerpt_length( $length){
	return 1000000;
}

add_theme_support( 'post-thumbnails' );
add_image_size( 'mailchimp', 564, 282, true);
//add_image_size( 'mailchimp-large', 560, 280, true);

add_action('tribe_events_after_the_content', 'weeplanit_after_content');
function weeplanit_after_content(){
	echo('<table cellpadding="5	" cellspacing="30" height="10-%" width="100%">');
	echo(weeplanit_get_ages(false));
	echo(weeplanit_get_cost2(false));
	echo(weeplanit_get_venue(false));
	echo(weeplanit_get_languages(false));
	echo(weeplanit_get_share());
	echo('</table>');
}

remove_action('tribe_events_after_footer', array('Tribe__Events__iCal','maybe_add_link'));



function weeplanit_tribe_event_featured_image_size( $size, $post_id) {
	if(is_feed()){
		//if(tribe_event_in_category('mailchimp-upcoming-events', null) || tribe_event_in_category('did-you-know', null))
		//	return "mailchimp-large";
		return "mailchimp";
	}
	return $size;
}
add_filter( 'tribe_event_featured_image_size', 'weeplanit_tribe_event_featured_image_size',10,2 );


	function weeplanit_pre_get_posts( $query ) {
		if( is_admin() ) {	
			return $query;
		}
		if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'tribe_events' ) {	
			$query->set('orderby', 'meta_value');	
			$query->set('meta_key', '_ecp_custom_28');
			$query->set('order', 'ASC'); 
		}
		// return
		return $query;

	}
	add_action('pre_get_posts', 'weeplanit_pre_get_posts', 20000, 1);



function tribe_venues_custom_field_support( $args ) {
	$args['supports'][] = 'custom-fields';
	return $args;
}
add_filter( 'tribe_events_register_venue_type_args', 'tribe_venues_custom_field_support' );


add_filter('tribe_get_event_website_link_label', 'tribe_get_event_website_link_label_default');
function tribe_get_event_website_link_label_default ($label) {
	global $weeplanit_language;
	if($weeplanit_language ==  'fr'){
		$label = "Visitez le site web &raquo;";
	}else{
		$label = "Visit Website &raquo;";
	}
	if( $label == tribe_get_event_website_url() ) {
		$label = "Visit Website &raquo;";
	}

	return '<a target="_blank" href="' . tribe_get_event_website_url() . '">' . $label . ' </a>';
}

add_filter('tribe_events_event_recurring_info_tooltip', 'weeplanit_recurrent_tooltip');
function weeplanit_recurrent_tooltip($tooltip ) {
			$tooltip = '<div class="recurringinfo">';
			$tooltip .= '<div class="event-is-recurring">';
			$tooltip .= tribe_get_recurrence_text( $post_id );
			$tooltip .= '</div>';
			$tooltip .= '</div>';
			return $tooltip;
}

add_filter('tribe_events_event_schedule_details_formatting', 'weeplanit_schedule_formatting');
function weeplanit_schedule_formatting( array $format ) {
	$format['datetime_separator'] = '<br>'; 
	global $weeplanit_language;
	$format['time_format'] = ($weeplanit_language == 'fr' ? 'G:i' : 'g:i a');
	if(tribe_event_is_multiday()){
		$format['date_without_year_format']= ($weeplanit_language == 'fr' ? 'j F' : 'F j');
	}else{	
		$format['date_without_year_format']= ($weeplanit_language == 'fr' ? 'l j F' : 'l F jS');
	}
	return $format;
}

add_filter('tribe_events_event_schedule_details_inner', 'weeplanit_schedule_details_inner',10,2);
function weeplanit_schedule_details_inner($inner, $eventId) {
	if ( is_null( $event ) ) {
		global $post;
		$event = $post;
	}
	global $weeplanit_language;
	if($weeplanit_language == "fr"){
		$inner = str_replace(":", " h ", $inner);
	}
	$fields = tribe_get_custom_fields();
	if (array_key_exists('All Year Long', $fields) && $fields['All Year Long'] == 'yes') {
		$inner = "";
	}
	if($weeplanit_language == 'fr' && array_key_exists('Time in French', $fields)){
		$inner =  $inner . '<br>'.$fields['Time in French'];
	}elseif($weeplanit_language == 'en' && array_key_exists("Time in English", $fields)){
		$inner = $inner . '<br>'.$fields['Time in English'];
	}else{
		if(array_key_exists("Friday Time", $fields)){
			$inner = $inner . '<br>'.($weeplanit_language == 'fr' ? 'Vendredi: ' : 'Friday: ').$fields['Friday Time'];
		}
		if(array_key_exists("Saturday Time", $fields)){
			$inner = $inner . '<br>'.($weeplanit_language == 'fr' ? 'Samedi: ' : 'Saturday: ').$fields['Saturday Time'];
		}
		if(array_key_exists("Sunday Time", $fields)){
			$inner = $inner . '<br>'.($weeplanit_language == 'fr' ? 'Dimanche: ' : 'Sunday: ').$fields['Sunday Time'];
		}
	}
	return $inner;
}

add_filter('tribe_get_cost', 'weeplanit_get_cost',10,3);
function weeplanit_get_cost($cost, $post_id, $with_currency_symbol) {
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	$cost = null;
	if (array_key_exists('Cost', $fields)) {
		if($weeplanit_language == 'fr'){
			$fields['Cost'] = str_replace('Free', 'Gratuit', $fields['Cost']);
			$fields['Cost'] = str_replace('Included with admission', "Inclus avec le droit d'entrée", $fields['Cost']);
			$fields['Cost'] = str_replace('Included with membership', "Inclus avec l'abonnement", $fields['Cost']);
		}
   	$cost =  str_replace(", ", ' - ', $fields['Cost']);
	}
	if(array_key_exists('Cost in English', $fields) && $weeplanit_language == 'en'){
		$cost .= ($cost == null ? '' : ' - ' ) . $fields["Cost in English"];
	}elseif(array_key_exists("Cost in French", $fields) && $weeplanit_language == 'fr'){
		$cost .= ($cost == null ? '' : ' - ' ) . $fields["Cost in French"];
	}
	return $cost;
}

function weeplanit_get_ages($plainText = false){
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	$ages = null;
	if (array_key_exists('Age', $fields)) {
		$ages = explode(', ', $fields['Age']);
		$min = min($ages);
		$max = max($ages);
		if($min == 0 && $max == 15)
			$ages = ($weeplanit_language == 'fr' ? "pour tous" : "for all");
		elseif($max == 15)
			$ages = $min . ($weeplanit_language == 'fr' ? " ans et plus" : " and up");
		elseif($min == 0)
			$ages = $max . ($weeplanit_language == 'fr' ? " ans et moins" : " and under");
		else
			$ages =  min($ages) . '-' . max($ages) . ($weeplanit_language == 'fr' ? " ans" : " y.o.");
	}
	if($ages != null){
		return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/9cd3d77e-e185-4404-94e1-ae2f03877edc.png', $ages, $plainText);
	}
	return;
}

function weeplanit_get_title(){
	$link = tribe_get_event_link(null, false);
	if($link != null){
		return '<a href="'.$link.'" style="text-decoration: none;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank">
				 <h4 style="color:'.$color.'">
				'.get_the_title_rss().'
				</h4>
				</a>';
	}else{
		return '<h4 style="color:'.$color.'">
				'.get_the_title_rss().'
				</h4>';
		}
}

function weeplanit_get_types($plainText = false){
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	if (array_key_exists('Type', $fields)) {
		if($weeplanit_language == 'fr'){
			$fields['Type'] = str_replace('Workshop', 'atelier', $fields['Type']);
			$fields['Type'] = str_replace('Show', 'spectacle', $fields['Type']);
			$fields['Type'] = str_replace('Museum', 'musée', $fields['Type']);
		}
	}
	if($fields['Type'] != null){
		return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/4f9030e0-60d2-4952-9e8b-15c8d7366124.png', $fields['Type'], $plainText);
	}
	return;
}

function weeplanit_get_themes($plainText = false){
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	if (array_key_exists('Theme', $fields)) {
		if($weeplanit_language == 'fr'){
			$fields['Theme'] = str_replace('History', 'histoire', $fields['Theme']);
			$fields['Theme'] = str_replace('Animals', 'animaux', $fields['Theme']);
			$fields['Theme'] = str_replace('Cooking', 'cuisine', $fields['Theme']);
			$fields['Theme'] = str_replace('Circus', 'cirque', $fields['Theme']);
			$fields['Theme'] = str_replace('Music', 'musique', $fields['Theme']);
			$fields['Theme'] = str_replace('Game', 'jeux', $fields['Theme']);
		}
	}
	if($fields['Theme'] != null){
		return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/4f9030e0-60d2-4952-9e8b-15c8d7366124.png', $fields['Theme'], $plainText);
	}
	return;
}

function weeplanit_get_languages($plainText = false){
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	if (array_key_exists('Language', $fields)) {
		$fields['Language'] = str_replace('French, English', 'Bilingual', $fields['Language']);
		if($weeplanit_language == 'fr'){
			$fields['Language'] = str_replace('French', 'français', $fields['Language']);
			$fields['Language'] = str_replace('English', 'anglais', $fields['Language']);
			$fields['Language'] = str_replace('Bilingual', 'bilingue', $fields['Language']);
		}
	}
	if($fields['Language'] != null){
		return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/4f9030e0-60d2-4952-9e8b-15c8d7366124.png', $fields['Language'], $plainText);
	}
	return;
}

function weeplanit_get_date($plainText = false){
	$date = tribe_events_event_schedule_details( null, '', '' );
	return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/1fe573e5-1516-4c11-851d-46eb6dba8170.png', $date.weeplanit_get_lastday(), $plainText);
}

function weeplanit_get_venue($plainText = false){
	global $weeplanit_language;
	$venue = tribe_get_venue();
	$city = tribe_get_city();
	if($venue != null){
		$venue = $venue . (tribe_show_google_map_link() ? ' - <a href="'.tribe_get_map_link().'">'.($city != null ? $city : "map").'</a>' : '');
		return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/d6bcecd4-35d5-4318-bb49-b462575cac95.png', $venue, $plainText);
	}
}

function weeplanit_get_lastday(){
	global $weeplanit_language;
	$fields = tribe_get_custom_fields();
	if (array_key_exists('Last Days', $fields) && $fields['Last Days'] == 'yes') {
		return "<div style='color:red;width:100%;'>" .($weeplanit_language == 'fr' ? "  Derniers jours!  " : "  Last Days!  ").'</div>';
	}
}

function weeplanit_get_cost2($plainText = false){
	$cost = tribe_get_cost(get_the_ID(), false);
	return weeplanit_get_one_detail('https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/d0597be8-60cb-4f58-8a3c-658ebdfa3d0d.png', $cost, $plainText);
}



function weeplanit_share_email(){
	global $weeplanit_language;
	$body = ($weeplanit_language == 'fr' ? "Salut, %0D%0A%0D%0AJ'ai reçu cette activité dans ma liste weeplanit, j'ai pensé à toi!" : "Hi, %0D%0A%0D%0AI've just received this activity in my Weeplanit's list. I thought you might be interested in it" ).'%0D%0A%0D%0A%0D%0A'.($weeplanit_language == 'fr' ? 'Titre' : 'Title' ).':%09'.get_the_title_rss().'%0D%0A
			Date:%09'.weeplanit_get_date(true).'%0D%0A
			'.($weeplanit_language == 'fr' ? 'Lieu' : 'Venue' ).':%09'.weeplanit_get_venue(true).'%0D%0A
			'.($weeplanit_language == 'fr' ? 'Âges' : 'Ages' ).':%09'.weeplanit_get_ages(true).'%0D%0A
			'.($weeplanit_language == 'fr' ? 'Coût' : 'Cost' ).':%09'.weeplanit_get_cost2(true).'%0D%0A
			'.($weeplanit_language == 'fr' ? 'Lien' : 'Link' ).':%09'.tribe_get_event_link(). '%0D%0A%0D%0A
			'.($weeplanit_language == 'fr' ? "Vous voulez recevoir votre propre liste d'activités comme votre ami(e)? inscrivez-vous gratuitement: http://wwww.weeplanit.com/fr" : 'You want to receive your own list of activities? register at http://www.weeplanit.com' );

	return '<a href="mailto:?Subject=Weeplanit: '.get_the_title_rss().'&body='.$body.'" target="_top" style="text-decoration:none; color:#fe7e00"><img class="shareIcone" height="24" style="width: 24px; height: 24px; margin: 0px;" width="24" src="https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/7266c4f1-e0da-4702-bcfc-74ecf7d594c3.png"/></a>';
}

function weeplanit_share_sms(){
	global $weeplanit_language;
	return '<a href="sms:?&body='.($weeplanit_language == "fr" ? "Je vais faire cette sortie en famille. Veux-tu te joindre? " :"We are going there. Do you want to join us? ") .tribe_get_event_link(). '"><img class="shareIcone" height="24" style="width: 24px; height: 24px; margin: 0px;" width="24" src="https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/1daa31ab-5a48-4957-a4d7-859a2b29ef37.png"/></a>';

}

function weeplanit_share_facebook(){

	$fb = '<div id="shareBtn'.get_the_ID().'"><img class="shareIcone" height="24" style="width: 24px; height: 24px; margin: 0px;" width="24" src="https://gallery.mailchimp.com/baad99a75ee83b1cff8d9c012/images/8aa9c952-34f7-4fae-8272-a45a31e37752.png"/></div>';
	$fb .= '<script>
		document.getElementById("shareBtn'.get_the_ID().'").onclick = function() {
  	FB.ui({
  	  method: "share",
  	  display: "popup",
  	  href: "'.tribe_get_event_link().'",
  	}, function(response){});
	}
	</script>';
	return $fb;
}

function weeplanit_get_share(){
	return '<table align="center" width="70px"><tr>
			<td align="left">'.weeplanit_share_sms().'</td>
			<td align="center">'.weeplanit_share_email().'</td>
			<!--<td align="right">'.weeplanit_share_facebook().'</td>-->
			</tr></table>';
}

function weeplanit_get_edit_button(){
	$actualUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

	if (strpos($actualUrl, 'edit=1') !== false) {
		$fields = tribe_get_custom_fields();
		$priority = $fields['Priority'];
		$link = "http://".$_SERVER[HTTP_HOST]."/wp-admin/post.php?post=".get_the_ID()."&amp;action=edit";
		return "<a href='".$link."'>edit</a> priority: " . $priority;
	}
}

function weeplanit_get_detail_button($color = '#fe7e00', $margin = 50){
	global $weeplanit_language;
	$link = tribe_get_event_link();
	if($link != null){
		return '<div style="text-align: center;background-color:'.$color.';margin:10px '.$margin.'px;">
					<a class="btn" href="'.$link.'" style="display: inline-block;padding: 7px 8px;text-align: center;text-decoration: none;opacity: 0.9;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" target="_blank">
						<span style="color:#FFFFFF">'.($weeplanit_language == 'fr' ? 'Visitez le site web' : 'Visit the website').'</span>
					</a>
				</div>';
	}
}

function weeplanit_get_one_detail($image_url, $detail, $plainText = false){
	if(!isset($detail) || $detail == '')
		return;
	if($plainText)
		return strip_tags($detail);
	return '<tr>
				<td valign="top" width="24px">
	        		<img class="imageIcone" align="none" height="16" src="'.$image_url.'" style="width: 16px; height: 16px; margin: 0px;" width="16" />
	           	</td>
	            <td>
	            	'.$detail.'
	           </td>
       	</tr>';
}

function weeplanit_get_font_color(){
	$color = "#fe7e00";
	if(tribe_event_in_category('mailchimp-upcoming-events', null) || tribe_event_in_category('did-you-know', null))
		$color = "#235789";
	elseif(tribe_event_in_category('mailchimp-recurrent-events', null))
			$color = "#fe7e00"; 
	return $color;
}

function weeplanit_content_rss($content){
	if(get_post_type() == "tribe_events"){
		global $weeplanit_language;
		$content = file_get_contents(get_stylesheet_directory() . '/mailchimpTemplate.html');
		$content = str_replace("|WEEPLANIT_IMAGE_URL|", wp_get_attachment_image_url(get_post_thumbnail_id(null), "mailchimp"), $content);
		$content = str_replace("|WEEPLANIT_TITLE|", get_the_title_rss(), $content);
		$content = str_replace("|WEEPLANIT_ORGANIZER|", tribe_get_venue(), $content);
		$content = str_replace("|WEEPLANIT_DESCRIPTION|", get_the_excerpt(), $content);
		$content = str_replace("|WEEPLANIT_AGE|", getTagTemplate(weeplanit_get_ages(true)), $content);
		$content = str_replace("|WEEPLANIT_LANGUAGE|", getTagTemplate(weeplanit_get_languages(true)), $content);
		$types = weeplanit_get_types(true);
		$str = "";
		$types = explode(", ", $types);
		foreach($types as $type){
			if($type != '')
				$str .= getTagTemplate($type);
		}
		$content = str_replace("|WEEPLANIT_TYPES|", $str, $content);
		$themes = weeplanit_get_themes(true);
		$str = "";
		$themes = explode(", ", $themes);
		foreach($themes as $theme){
			if($theme != '')
			$str .= getTagTemplate($theme);
		}
		$content = str_replace("|WEEPLANIT_THEMES|", $str, $content);
		$fields = tribe_get_custom_fields();
		$friday = $fields['Friday Time'];
		$str = "";
		if($friday != "")
			$str .= '<tr>
                        <td style="font-size:10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.($weeplanit_language == 'fr' ? "Vendredi" : "Friday").': </h4></td>
                        <td style="font-size:10px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.$friday.'</h4></td>
                    </tr>';
        $saturday = $fields['Saturday Time'];
        if($saturday != "")
			$str .= '<tr>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.($weeplanit_language == 'fr' ? "Samedi" : "Saturday").': </h4></td>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.$saturday.'</h4></td>
                    </tr>';
        $sunday = $fields['Sunday Time'];
        if($sunday != "")
			$str .= '<tr>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.($weeplanit_language == 'fr' ? "Dimanche" : "Sunday").': </h4></td>
                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><h4>'.$sunday.'</h4></td>
                    </tr>';
        $content = str_replace("|WEEPLANIT_DATES|", $str, $content);  
        $content = str_replace("|WEEPLANIT_COST|", weeplanit_get_cost2(true), $content);      

        $str = '<a class="mcnButton " title="'.($weeplanit_language == "fr" ? "Visitez le site web" : "Visit the website").'" href="'.tribe_get_event_link().'" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration:none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">'.($weeplanit_language == "fr" ? "Visitez le site web" : "Visit the website").'</a>';
        $content = str_replace("|WEEPLANIT_LINK|", $str, $content);                

		return $content;
	} 
	return $content;
}
add_filter( "the_content_feed", "weeplanit_content_rss" );

function getTagTemplate($tag) {
	return '<!--[if mso]>
                              <td align="center" valign="top">
                                 <![endif]-->
                                 <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                       <tr>
                                          <td valign="top" style="padding: 1px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnShareContentItemContainer">
                                             <table border="0" cellpadding="0" cellspacing="0" width="" class="mcnShareContentItem" style="border-collapse: separate;border-radius: 3px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody>
                                                   <tr>
                                                      <td align="left" valign="middle" style="padding:1px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                         <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-style: solid;border-width: 1px;
                                                            border-color: #235789;border-collapse: separate !important;border-radius: 5px;background-color:#fff;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                               <tr style="">
                                                                  <td align="left" valign="middle" class="mcnShareTextContent" style="padding:1px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                     <a href="http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Feepurl.com%2FcnA3jP" target="" style="color: #235789;font-family: &quot;Lucida Sans Unicode&quot;, &quot;Lucida Grande&quot;, sans-serif;font-size: 10px;font-weight: bold;line-height: normal;text-align: center;text-decoration: none;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">'.$tag.'</a>
                                                                  </td>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 <!--[if mso]>
                              </td>
                              <![endif]-->';
}

function weeplanit_categories_rss( $list, $type ) {
	$current_year = date("Y");
	$ages = get_post_meta(get_the_ID(), '_ecp_custom_2');
	$ages = array_reverse(explode("|", $ages[0]));
	foreach ($ages as $age){
		$age = (int)$age;
		$list .= '<category>'.($current_year-$age).'</category>';
	}
	//return '<category>'.get_the_term_list(get_the_ID(), 'tribe_events_cat').'</category>';
	return $list;
}
add_filter( 'the_category_rss', 'weeplanit_categories_rss', 10, 2 ); 

add_filter('password_protected_login_headerurl', 'changeRedirectURL');
function changeRedirectURL(){
	return "http://www.weeplanit.com";
}
add_filter( 'wp_trim_words', 'tribe_preserve_html_in_excerpt', 10, 4 );
function tribe_preserve_html_in_excerpt( $text, $num_words, $more, $original_text ) {
	
	remove_filter( 'wp_trim_words', 'tribe_preserve_html_in_excerpt', 10 );
	
	$excerpt = force_balance_tags( html_entity_decode( wp_trim_words( htmlentities( $original_text ) ) ) );
	
	add_filter( 'wp_trim_words', 'tribe_preserve_html_in_excerpt', 10, 4 );
	
	return $excerpt;
}

function my_enqueue($hook) {
   wp_enqueue_script( 'my_custom_script', get_stylesheet_directory_uri() . '/js/admin_script.js' );

}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );

// Our custom post type function
function create_posttype() {

	register_post_type( 'weeplanit_datasource',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Datasources' ),
				'singular_name' => __( 'Datasource' )
			),
			'public' => false,
			'has_archive' => true,
			'show_in_menu' => true,
			'show_ui' => true,
			'menu_icon' => 'dashicons-archive',
			'rewrite' => array('slug' => 'datasources'),
		)
	);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

add_filter( 'manage_weeplanit_datasource_posts_columns', 'set_custom_edit_weeplanit_datasource_columns' );
add_action( 'manage_weeplanit_datasource_posts_custom_column' , 'custom_weeplanit_datasource_column', 10, 2 );

function set_custom_edit_weeplanit_datasource_columns($columns) {
   unset($columns['language']);
   unset($columns['date']);
   $columns['description'] = __( 'URL', 'your_text_domain' );
   return $columns;
}

function custom_weeplanit_datasource_column( $column, $post_id ) {
   switch ( $column ) {


       case 'description' :
		$content_post = get_post($post_id);
		$content = $content_post->post_content;
      		echo '<a href="'.$content.'" target="_blank">'.$content.'</a>';
           	break;

   }
}
/**
* Eventica Child Theme functions and definitions.
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

add_action( 'after_setup_theme', 'tokopress_load_childtheme_languages', 5 );
function tokopress_load_childtheme_languages() {
	/* this theme supports localization */
	load_child_theme_textdomain( 'tokopress', get_stylesheet_directory() . '/languages' );	
}



/* Please add your custom functions code below this line. */
